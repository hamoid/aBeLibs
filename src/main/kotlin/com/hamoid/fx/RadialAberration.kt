package com.hamoid.com.hamoid.fx

import org.openrndr.application
import org.openrndr.color.rgb
import org.openrndr.draw.*
import org.openrndr.drawImage
import org.openrndr.extra.parameters.Description
import org.openrndr.extra.parameters.DoubleParameter
import org.openrndr.resourceUrl
    
/**
 * id: c13dc8bf-0627-49cf-a0c1-34760566e76a
 * description: New sketch
 * tags: #new
 */    

@Description("Radial Aberration")
class RadialAberration : Filter(
    filterShaderFromUrl(
        resourceUrl("/shaders/radial-aberration.frag")
    )
) {
    @DoubleParameter("strength", 0.0, 1.0, order = 10)
    var strength: Double by parameters

    init {
        strength = 0.004
    }
}

/** Test RadialAberration */
fun main() = application {
    program {
        val radialAberration = RadialAberration()
        val result = colorBuffer(width, height)
        val src = drawImage(width, height) {
            for(r in 400 downTo 10 step 40) {
                fill = rgb((r * 1.3579) % 1.0)
                circle(bounds.center, r * 1.0)
            }
        }
        extend {
            radialAberration.strength = 0.002
            radialAberration.apply(src, result)
            drawer.image(result)
        }
    }
}
