package com.hamoid.com.hamoid.fx

import org.openrndr.application
import org.openrndr.color.rgb
import org.openrndr.draw.*
import org.openrndr.drawImage
import org.openrndr.extra.imageFit.FitMethod
import org.openrndr.extra.imageFit.imageFit
import org.openrndr.extra.noise.Random
import org.openrndr.extra.noise.uniform
import org.openrndr.extra.parameters.Description
import org.openrndr.extra.parameters.IntParameter
import org.openrndr.math.Polar
import org.openrndr.resourceUrl
    
/**
 * id: c13dc8bf-0627-49cf-a0c1-34760566e76a
 * description: New sketch
 * tags: #new
 */    

// See https://blog.maximeheckel.com/posts/on-crafting-painterly-shaders/

@Description("Painterly")
class Painterly : Filter(
    filterShaderFromUrl(
        resourceUrl("/shaders/painterly.frag")
    )
) {
    @IntParameter("radius", 1, 10, order = 10)
    var radius: Int by parameters

    init {
        radius = 3
    }
}

/** Test Painterly */
fun main() = application {
    program {
        val painterly = Painterly()
        val src = drawImage(width, height) {
            val img = loadImage("/home/funpro/datasea/Pictures/2024/04/IMG_20240416_085247.jpg")
            //imageFit(img, bounds, fitMethod = FitMethod.Cover)
            img.destroy()

            for(r in 400 downTo 10 step 40) {
                fill = null
                strokeWeight = 40.0
                stroke = rgb(
                    (r * 1.11) % 1.0,
                    (r * 1.71) % 1.0,
                    (r * 1.41) % 1.0,
                    0.5
                )
                circle(bounds.center, r * 1.0)
            }
            repeat(360) {
                strokeWeight = 4.0
                stroke = rgb(Double.uniform(0.0, 1.0), Random.double(0.1, 0.2))
                lineSegment(bounds.center, Polar(it.toDouble(), 1000.0).cartesian)
            }
        }
        val result = colorBuffer(width, height, type = ColorType.FLOAT32)
        extend {
            painterly.radius = 3
            if(mouse.position.x > width / 2) {
                painterly.apply(src, result)
            } else {
                src.copyTo(result)
            }
            drawer.image(result)
        }
    }
}
