package com.hamoid.com.hamoid.geometry

import com.hamoid.data.uniquePairs
import org.openrndr.extra.shapes.rectify.rectified
import org.openrndr.math.Vector2
import org.openrndr.shape.ShapeContour
import org.openrndr.shape.split

private data class ContourExtrema(
    val contour: ShapeContour,
    val contourT: Double,
    val position: Vector2,
    val normal: Vector2,
    val index: Int
)

private fun ShapeContour.at(index: Int, ut: Double) =
    ContourExtrema(this, ut, position(ut), normal(ut), index)

fun MutableList<ShapeContour>.splitAll() {
    var intersectionsFound: Boolean
    main@ do {
        intersectionsFound = false
        for (caIndex in 0 until size) {
            val ca = this[caIndex]
            for (cbIndex in caIndex until size) {
                val cb = this[cbIndex]

                // Almost 2x speedup
                if(!ca.bounds.intersects(cb.bounds)) continue

                val parts = ca.split(cb) + cb.split(ca)

                if (parts.size > 2) {
                    removeAll(setOf(ca, cb))
                    addAll(parts)
                    intersectionsFound = true
                    continue@main
                }
            }
        }
    } while (intersectionsFound)
}

private fun clusterContourExtrema(contours: List<ShapeContour>, eps: Double): Map<Int, List<ContourExtrema>> {
    val clusters = mutableMapOf<Int, MutableList<ContourExtrema>>()
    var clusterId = 0

    fun addToCluster(index: Int, c: ShapeContour, t: Double) {
        val pos = c.position(t)
        val hit = clusters.filter { (k, v) ->
            pos.distanceTo(v.first().position) < eps
        }
        if (hit.isEmpty()) {
            clusters[clusterId] = mutableListOf(c.at(index, t))
            clusterId++
        } else {
            clusters[hit.keys.first()]!!.add(c.at(index, t))
        }

    }

    contours.forEachIndexed { i, c ->
        addToCluster(i, c, 0.0)
        addToCluster(i, c, 1.0)
    }

    return clusters
}

fun MutableList<ShapeContour>.shortenToSimulateDepth(length: Double) {
    val clusters = clusterContourExtrema(this, 1.1)
    val shorten = mutableSetOf<Pair<Int, Double>>()
    // based on clusters, decide which ones to shorten
    clusters.forEach { (_, xtr) ->
        // Find two aligned items in xtr to not shorten
        val keepLong = xtr.uniquePairs().shuffled().firstOrNull { (a, b) ->
            a.normal.dot(b.normal) > 0.95
        }?.toList()
        val toShorten = xtr.toMutableList()
        keepLong?.let { toShorten.removeAll(it) }

        shorten.addAll(
            toShorten.map {
                Pair(it.index, it.contourT)
            }
        )
    }
    shorten.forEach { (which, contourT) ->
        val t = length / this[which].length
        val (a, b) = if (contourT > 0.5)
            listOf(0.0, 1.0 - t) else listOf(t, 1.0)
        this[which] = this[which].rectified().sub(a, b)
    }
}
