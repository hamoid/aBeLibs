package com.hamoid.com.hamoid.fx

import org.openrndr.application
import org.openrndr.draw.*
import org.openrndr.extra.noise.filters.SimplexNoise3D
import org.openrndr.math.Vector3
import org.openrndr.shape.Rectangle
    
/**
 * id: 7f0d5b8d-b612-4f95-aebe-9dd9a643b168
 * description: New sketch
 * tags: #new
 */    

/**
 * RGB clouds, animated.
 */
class NoiseRGB : Filter1to1() {
    var seed = 0.0
    var add = 0.0
    var mult = 1.0
    private val yShift = Vector3(0.0, 0.1, 0.2)
    private val seedShift = Vector3(0.0, 0.2, 0.4)

    private val simplex3D = SimplexNoise3D().also {
        it.octaves = 2
        it.scale = Vector3(0.3)
    }
    private var cloudR: ColorBuffer? = null
    private var cloudG: ColorBuffer? = null
    private var cloudB: ColorBuffer? = null

    private val joinRGB = JoinRGB()

    private fun initBuffers(src: ColorBuffer) {
        cloudR?.let {
            if (!it.isEquivalentTo(src)) {
                it.destroy()
            }
        }
        if (cloudR == null) {
            cloudR = src.createEquivalent()
        }

        cloudG?.let {
            if (!it.isEquivalentTo(src)) {
                it.destroy()
            }
        }
        if (cloudG == null) {
            cloudG = src.createEquivalent()
        }

        cloudB?.let {
            if (!it.isEquivalentTo(src)) {
                it.destroy()
            }
        }
        if (cloudB == null) {
            cloudB = src.createEquivalent()
        }
    }

    override fun apply(source: Array<ColorBuffer>, target: Array<ColorBuffer>, clip: Rectangle?) {
        initBuffers(target[0])

        simplex3D.seed = Vector3(seed, yShift.x, seed + seedShift.x)
        simplex3D.apply(emptyArray(), cloudR!!)

        simplex3D.seed = Vector3(seed, yShift.y, seed + seedShift.y)
        simplex3D.apply(emptyArray(), cloudG!!)

        simplex3D.seed = Vector3(seed, yShift.z, seed + seedShift.z)
        simplex3D.apply(emptyArray(), cloudB!!)

        joinRGB.add = add
        joinRGB.mult = mult
        joinRGB.apply(arrayOf(cloudR!!, cloudG!!, cloudB!!), target)
    }
}

fun main() = application {
    program {
        val w = width
        val h = height
        val noise = NoiseRGB()
        val result = colorBuffer(w, h)
        extend {
            noise.seed = seconds
            noise.apply(result, result)
            drawer.image(result)
        }
    }
}
