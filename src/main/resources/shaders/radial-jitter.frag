in vec2 v_texCoord0;

uniform float minDist;
uniform float maxDist;
uniform float minBlur;
uniform float maxBlur;
uniform float noiseAmount;

out vec4 o_color;

float rand(vec2 co){
    return fract(sin(dot(co.xy, vec2(12.9898, 78.233))) * 43758.5453);
}

void main() {
    vec2 uv = v_texCoord0 - 0.5;

    float k = smoothstep(minDist, maxDist, length(uv));

    float amt = mix(minBlur, maxBlur, k) * (1.0 + rand(uv) * noiseAmount);

    o_color = vec4(normalize(uv), 0.0, 1.0) * amt;
}