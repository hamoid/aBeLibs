package com.hamoid.com.hamoid.geometry

import org.openrndr.application
import org.openrndr.math.Vector2
import org.openrndr.shape.Circle
import org.openrndr.shape.ShapeContour
import org.openrndr.shape.contains
import kotlin.math.sqrt
    
/**
 * id: ef442d22-810a-4f90-b34f-b9d31f7ca460
 * description: New sketch
 * tags: #new
 */    

fun hexGrid(contour: ShapeContour, radius: Double): List<Vector2> {
    val result = mutableListOf<Vector2>()
    val b = contour.bounds
    val w = 2 * radius
    val h = sqrt(3.0) * radius
    repeat((b.height / h).toInt()) { yy ->
        val y = b.y + yy * h
        repeat((b.width / w).toInt()) { xx ->
            val x = b.x + xx * w + if(yy % 2 == 0) 0.0 else radius
            result.add(Vector2(x, y))
        }

    }
    return result //.filter { contour.contains(it) }
}

fun main() = application {
    program {
        val c = Circle(drawer.bounds.center, 200.0).contour
        val pts = hexGrid(c, 60.0)
        extend {
            drawer.contour(c)
            drawer.circles(pts, 5.0)
        }
    }
}
