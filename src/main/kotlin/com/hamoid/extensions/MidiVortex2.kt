package com.hamoid.extensions

import com.hamoid.math.InterpolatorDouble
import org.openrndr.Extension
import org.openrndr.Program
import org.openrndr.draw.Drawer
import org.openrndr.extra.midi.*
import kotlin.reflect.KMutableProperty

annotation class VtxCcAction(
    val name: String, val ch: Int, val ccnum: Int
)

annotation class VtxNoteAction(
    val name: String, val ch: Int, val note: Int
)

annotation class VtxCcSigned(
    val name: String, val ch: Int, val ccnum: Int
)

annotation class VtxCcUnsigned(
    val name: String, val ch: Int, val ccnum: Int
)

annotation class VtxBendSigned(
    val name: String, val ch: Int
)

/**
 * Midi vortex2 extension.
 * Usage: `extend(MidiVortex2()) { add(midivars) }`
 * I can receive NOTE_ON, CONTROL_CHANGED, PITCH_BEND, PRESSURE
 * Future: figure out a better way for mapping midi events to variables,
 * interpolators or methods. Currently, I use [makeId] for the mapping but
 * that's kind of hacky and not flexible.
 */
class MidiVortex2(program: Program) : Extension {
    override var enabled: Boolean = true

    /**
     * Associates a type of MIDI event with a variable or method to target
     */
    private val targets = mutableMapOf<Int, Any>()
    private val interpolators = mutableListOf<InterpolatorDouble>()

    /**
     * [controller] holds the open MIDI device or null if it can't be found.
     */
    private val controller = try {
        MidiDeviceDescription.list().forEach(::println)
        MidiDeviceDescription.list().firstOrNull { it.name.contains("V2") }
            ?.run {
                MidiTransceiver.fromDeviceVendor(program, name, vendor)
            }
    } catch (e: IllegalArgumentException) {
        null
    }

    init {
//        @VtxCcUnsigneds("F1 slider", 0, 14, 21)
//        var sliders = List(21 - 14 + 1) {
//            Interpolator(0.0, 0.01, 0.05, 0.4)
//        }

        controller?.run {
            // ╔╦╗╦╔╦╗╦  ╦═╗╔═╗╔═╗╔═╗╦╦  ╦╔═╗
            // ║║║║ ║║║  ╠╦╝║╣ ║  ║╣ ║╚╗╔╝║╣
            // ╩ ╩╩═╩╝╩  ╩╚═╚═╝╚═╝╚═╝╩ ╚╝ ╚═╝
            val deliverFunc = { it: MidiEvent ->
                /**
                 * We need a way to map MidiEvents to variables.
                 * We use [targets] for that mapping.
                 */
                val id = makeId(it.eventType, it.channel, it.control + it.note)

                // Only one variable is set, others are 0.
                val v =
                    (it.value + it.pitchBend + it.pressure + it.velocity) / 127.0

                when (val target = targets[id]) {
                    is MidiActionFun -> {
                        target.method.call(target.obj, v); Unit
                    }

                    is MidiSignedVar ->
                        target.prop.setter.call(target.obj, v * 2 - 1)

                    is MidiDoubleVar ->
                        target.prop.setter.call(target.obj, v)

                    is MidiDoubleVarSmooth ->
                        target.iptor.target = v

                    is MidiSignedVarSmooth ->
                        target.iptor.target = v * 2 - 1

                    else -> println(it)
                }
            }

            noteOn.postpone = true
            noteOn.listen(deliverFunc)

            controlChanged.postpone = true
            controlChanged.listen(deliverFunc)

            pitchBend.postpone = true
            pitchBend.listen(deliverFunc)

            channelPressure.postpone = true
            channelPressure.listen(deliverFunc)
        }
    }

    /**
     * Deliver events and update all interpolators
     */
    override fun beforeDraw(drawer: Drawer, program: Program) {
        controller?.run {
            noteOn.deliver()
            controlChanged.deliver()
            pitchBend.deliver()
            channelPressure.deliver()
        }

        interpolators.forEach { it.update() }
    }

    /**
     * Make mapping ID for internal use.
     */
    private fun makeId(type: MidiEventType, a: Int, b: Int) =
        (type.ordinal shl 16) + (a shl 8) + b


    private fun printUnknown(name: String, ann: Any?) =
        println("Unknown var $name annotation $ann")

    /**
     * Populate [targets].
     * Add a collection of variables to be controlled with the MIDI controller.
     *
     * [vars] is an `object` with annotated variables. The annotations map
     * MIDI events to various instances of data classes.
     * They also specify if MIDI values should be decoded as
     * normalized, signed, smoothly interpolated over time or not.
     */
    fun add(vars: Any) {
        vars::class.members.forEach {
            it.annotations.forEach { ann ->
                when (ann) {

                    /* Used to execute a function when triggered */
                    is VtxCcAction -> {
                        val id = makeId(
                            MidiEventType.CONTROL_CHANGED, ann.ch, ann.ccnum
                        )
                        targets[id] = MidiActionFun(ann.name, vars, it)
                    }

                    is VtxNoteAction -> {
                        val id = makeId(
                            MidiEventType.NOTE_ON, ann.ch, ann.note
                        )
                        targets[id] = MidiActionFun(ann.name, vars, it)
                    }

                    /* Signed value */
                    is VtxCcSigned -> {
                        val id = makeId(
                            MidiEventType.CONTROL_CHANGED, ann.ch, ann.ccnum
                        )
                        if (it is KMutableProperty<*>) {
                            when (val v = it.getter.call(vars)) {
                                is Double ->
                                    targets[id] =
                                        MidiSignedVar(ann.name, vars, it)

                                is InterpolatorDouble -> {
                                    interpolators.add(v)
                                    targets[id] =
                                        MidiSignedVarSmooth(ann.name, v)
                                }
                            }
                        }
                    }

                    /* Unsigned value */
                    is VtxCcUnsigned -> {
                        val id = makeId(
                            MidiEventType.CONTROL_CHANGED, ann.ch, ann.ccnum
                        )
                        if (it is KMutableProperty<*>) {
                            when (val v = it.getter.call(vars)) {
                                is Double ->
                                    targets[id] =
                                        MidiDoubleVar(ann.name, vars, it)

                                is InterpolatorDouble -> {
                                    interpolators.add(v)
                                    targets[id] =
                                        MidiDoubleVarSmooth(ann.name, v)
                                }

                                else -> printUnknown(ann.name, v)
                            }
                        }
                    }

                    /* Signed value associated to pitch bend */
                    is VtxBendSigned -> {
                        val id = makeId(
                            MidiEventType.PITCH_BEND, ann.ch, 0
                        )
                        if (it is KMutableProperty<*>) {
                            when (val v = it.getter.call(vars)) {
                                is Double ->
                                    targets[id] =
                                        MidiSignedVar(ann.name, vars, it)

                                is InterpolatorDouble -> {
                                    interpolators.add(v)
                                    targets[id] =
                                        MidiSignedVarSmooth(ann.name, v)
                                }

                                else -> printUnknown(ann.name, v)
                            }
                        }
                    }

                }
            }
        }
    }

    /**
     * Send CC back to the device
     */
//    private fun sendControlChange(ch: Int, ccnum: Int, v: Int) {
//        controller?.controlChange(ch, ccnum, v)
//    }
}
