package com.hamoid.osc

enum class Tr {
    //DRUM, PAD, C, D, E, F, G
    MELODY, BASS, DRUM, PAD, DRONE, BRIDGE, TRIGGER
}