package com.hamoid.geometry

import org.openrndr.extra.noise.Random
import org.openrndr.math.Polar
import org.openrndr.math.Vector2
import org.openrndr.math.asDegrees
import org.openrndr.math.transforms.transform
import org.openrndr.panel.elements.round
import kotlin.math.atan2
import kotlin.math.max

//fun mix(a: Vector2, b: Vector2, x: Double): Vector2 {
//    return a * (1 - x) + b * x
//}
//
//fun Vector2.lerp(other: Vector2, x: Double): Vector2 {
//    return this * (1 - x) + other * x
//}

/**
 *
 */
fun Polar.rotated(theta: Double): Polar {
    return Polar(this.theta + theta, radius)
}

/**
 * Displace a vector using Simplex noise, using its
 * own pasition as input
 */
fun Vector2.noised(amt: Double = 1.0): Vector2 {
    return this + Vector2(
        Random.simplex(this),
        Random.simplex(this.yx + 3.17)
    ) * amt
}

/**
 * .round() but applied to Vector2
 */
fun Vector2.round(decimals: Int): Vector2 {
    return Vector2(x.round(decimals), y.round(decimals))
}

/**
 * Takes a list of points forming a contour and transforms the positions
 * of all points in such a way that the first one ends up at [start]
 * and the last one ends up at [end]. To achieve this it creates a
 * mat44 transform (translation, rotation and scale)
 */
fun List<Vector2>.orient(start: Vector2, end: Vector2): List<Vector2> {
    val points = this
    val n = points.first().distanceTo(points.last())
    val mat = transform {
        translate(start)
        scale(start.distanceTo(end) / max(n, 0.01))
        rotate((end - start).heading() - (points.last() - points.first()).heading())
        translate(-points.first())
    }
    return this.map {
        (mat * it.xy01).xy
    }
}

/**
 * Vector heading in degrees
 */
fun Vector2.heading() = atan2(y, x).asDegrees
