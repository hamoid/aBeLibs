in vec2 v_texCoord0;

uniform sampler2D tex0;
uniform vec2 textureSize0;

uniform int radius;

out vec4 o_color;

// NEXT: From Structure Tensor to Anisotropic Kuwahara filter
// now it will split the work into three passes.

#define SECTOR_COUNT 8

vec3 sampleColor(vec2 offset) {
    return texture(tex0, v_texCoord0 + offset / textureSize0).rgb;
}

float polynomialWeight(float x, float y, float eta, float lambda) {
    float polyValue = (x + eta) - lambda * (y * y);
    return max(0.0, polyValue * polyValue);
}

void getSectorVarianceAndAverageColor(float angle, float radius, out vec3 avgColor, out float variance) {
    vec3 colorSum = vec3(0.0);
    vec3 squaredColorSum = vec3(0.0);
    float weightSum = 0.0;

    float eta = 0.1;
    float lambda = 0.5;

    for (float r = 1.0; r <= radius; r += 1.0) {
        for (float a = -0.392699; a <= 0.392699; a += 0.196349) {
            vec2 sampleOffset = vec2(r * cos(angle + a), r * sin(angle + a));
            vec3 color = sampleColor(sampleOffset);
            float weight = polynomialWeight(sampleOffset.x, sampleOffset.y, eta, lambda);

            colorSum += color * weight;
            squaredColorSum += color * color * weight;
            weightSum += weight;
        }
    }

    // Calculate average color and variance
    avgColor = colorSum / weightSum;
    vec3 varianceRes = (squaredColorSum / weightSum) - (avgColor * avgColor);
    variance = dot(varianceRes, vec3(0.299, 0.587, 0.114)); // Convert to luminance
}

void main() {
    vec3 sectorAvgColors[SECTOR_COUNT];
    float sectorVariances[SECTOR_COUNT];

    for (int i = 0; i < SECTOR_COUNT; i++) {
        float angle = float(i) * 6.28318 / float(SECTOR_COUNT);
        getSectorVarianceAndAverageColor(angle, float(radius), sectorAvgColors[i], sectorVariances[i]);
    }

    float minVariance = sectorVariances[0];
    vec3 finalColor = sectorAvgColors[0];

    // find colors with minimum variances
    for (int i = 1; i < SECTOR_COUNT; i++) {
        if (sectorVariances[i] < minVariance) {
            minVariance = sectorVariances[i];
            finalColor = sectorAvgColors[i];
        }
    }

    o_color = vec4(finalColor, 1.0);
}
