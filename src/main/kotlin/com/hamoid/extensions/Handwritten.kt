package com.hamoid.extensions

import org.openrndr.Extension
import org.openrndr.Program
import org.openrndr.draw.Drawer
import org.openrndr.draw.LineJoin
import org.openrndr.draw.isolated
import org.openrndr.extra.composition.CompositionDrawer
import org.openrndr.math.Polar
import org.openrndr.math.Vector2
import org.openrndr.math.transforms.transform
import org.openrndr.shape.Rectangle
import org.openrndr.shape.ShapeContour
import org.openrndr.shape.bounds
import org.openrndr.extra.svg.loadSVG
import kotlin.math.max

class Handwritten : Extension {
    override var enabled: Boolean = true

    private val svg = loadSVG("data/text-template-3-plain.svg")
    private val bounds = mutableMapOf<Char, Rectangle>()
    private val letters = mutableMapOf<Char, MutableList<ShapeContour>>()
    private val lines = mutableListOf<Pair<String, Vector2>>()
    var scale = 1.0

    val lineCount: Int
        get() = lines.size

    fun add(txt: String, pos: Vector2, align: Vector2 = Vector2.ZERO) {
        var size = Vector2.ZERO
        txt.forEach { letter ->
            if(!letters.containsKey(letter) && letter.code != 32) {
                println("Letter not found ${letter.code} ($letter)")
            }
            size = letters[letter]?.run {
                val letterBounds = (this.map { it.bounds }).bounds
                Vector2(
                    size.x + letterBounds.width + 3,
                    max(letterBounds.height, size.y)
                )
            } ?: Vector2(size.x + 10.0, size.y)
        }

        lines.add(Pair(txt, pos - align * size))
    }

    fun clear() = lines.clear()

    @Suppress("unused")
    fun hasGlyph(letter: Char) = letters.containsKey(letter)

    override fun setup(program: Program) {
        svg.findShapes().filter { it.id!!.length <= 4 }.forEach {
            val id = it.id!!
            val idChar =
                if (id.length == 1) id.last() else id.substring(1).toInt()
                    .toChar()
            bounds[idChar] = it.bounds
        }
        svg.findShapes().filter { it.id!!.length > 4 }.forEach { curve ->
            for (b in bounds) {
                if (b.value.contains(curve.shape.contours.first().segments.first().start)) {
                    if (!letters.containsKey(b.key)) {
                        letters[b.key] = mutableListOf()
                    }
                    letters[b.key]!!.add(curve.shape.outline.transform(transform {
                        translate(-b.value.corner)
                    }))
                    break
                }
            }
        }
    }

    fun drawToSVG(svg: CompositionDrawer) {
        for ((line, pos) in lines) {
            svg.isolated {
                translate(pos)
                scale(scale)
                line.forEach { letter ->
                    letters[letter]?.run {
                        val letterBounds = (this.map { it.bounds }).bounds
                        translate(-letterBounds.x, 0.0)
                        contours(this)
                        translate(letterBounds.x + letterBounds.width + 3, 0.0)
                    } ?: translate(10.0, 0.0)
                }
            }
        }
    }

    fun draw(drawer: Drawer) {
        for ((line, pos) in lines) {
            drawer.isolated {
                translate(pos)
                scale(scale)
                lineJoin = LineJoin.BEVEL
                line.forEach { letter ->
                    letters[letter]?.run {
                        val letterBounds = (this.map { it.bounds }).bounds
                        translate(-letterBounds.x, 0.0)
                        contours(this)
                        translate(
                            letterBounds.x + letterBounds.width + 3.0,
                            0.0
                        )
                    } ?: translate(10.0, 0.0)
                }
            }
        }
    }

    fun drawToSVG(svg: CompositionDrawer, contours: List<ShapeContour>) {
        // I use `acc` to accumulate text characters that do not fit in
        // a given contour, so they are passed to the next contour. Otherwise,
        // text is lost if the contour is too short.
        var acc = ""
        lines.forEachIndexed lin@{ i, (line, _) ->
            if (i >= contours.size)
                return@lin

            val contour = contours[i]
            val len = contour.length
            var t = 0.0
            acc += line
            acc.forEachIndexed chr@{ ci, letter ->
                val pc = t / len
                if (pc > 1) {
                    acc = if(ci < acc.length) acc.substring(ci) else ""
                    return@chr
                }
                svg.isolated {
                    val l = letters[letter]
                    t += scale * if (l != null) {
                        val letterBounds = (l.map { it.bounds }).bounds
                        translate(contour.position(pc))
                        scale(scale)
                        rotate(Polar.fromVector(contour.normal(pc)).theta + 90)
                        translate(-letterBounds.x, -2 * scale)
                        contours(l)
                        (letterBounds.width + 3.0)
                    } else {
                        10.0
                    }
                }
            }
            acc = ""
        }
    }

    override fun afterDraw(drawer: Drawer, program: Program) {
    }
}
