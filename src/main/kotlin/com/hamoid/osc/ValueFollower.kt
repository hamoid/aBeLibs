package com.hamoid.osc

import org.openrndr.math.mix

data class ValueFollower(var curr: Double, var target: Double = curr) {
    fun update() {
        curr = mix(curr, target, 0.1)
    }
}