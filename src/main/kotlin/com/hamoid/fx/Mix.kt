package com.hamoid.com.hamoid.fx

import org.openrndr.draw.*
import org.openrndr.extra.parameters.Description
import org.openrndr.extra.parameters.DoubleParameter
import org.openrndr.resourceUrl

@Description("Mix")
class Mix : Filter(
    filterShaderFromUrl(resourceUrl("/shaders/mix.frag"))
) {
    @DoubleParameter("Mixer", 0.0, 1.0, order = 10)
    var mixer: Double by parameters

    init {
        mixer = 0.5
    }
}
