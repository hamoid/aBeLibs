in vec2 v_texCoord0;

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;

uniform float add;
uniform float mult;

out vec4 o_color;

void main() {
    vec2 uv = v_texCoord0;
    float r = texture(tex0, uv).r;
    float g = texture(tex1, uv).g;
    float b = texture(tex2, uv).b;
    float a = (r + g + b) / 3.0;
    o_color = vec4(r, g, b, a);
    o_color.rgb *= mult;
    o_color.rgb += add;
}
