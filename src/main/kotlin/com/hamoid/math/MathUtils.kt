package com.hamoid.math

import org.openrndr.math.Vector2
import kotlin.math.*

object MathUtils {
    /**
     * Rotates the start angle towards the end angle by amount over the
     * shorter direction (CW or CCW)
     * #OR
     *
     * @param start angle in radians
     * @param end angle in radians
     * @param t normalized value
     * @return the linearly interpolated angle
     */
    fun lerpAngle(start: Double, end: Double, t: Double): Double {
        var a = start
        var b = end
        // If difference greater than half turn
        if (abs(b - a) > Math.PI) {
            if (b > a) a += TAU else b += TAU
        }
        val value = a + (b - a) * t
        return if (value in 0.0..TAU) value else value % TAU
    }

    /**
     * Maps normalized [Double] to [ min..max ].
     * Example `t.denormalize(2.0, 4.0)`
     */
    fun Double.denormalize(min: Double, max: Double) = min + this * (max - min)

    /**
     * Convert angle to a 2D point belonging to a square
     * #OR
     *
     * @param radians
     * @param halfSide Radius of the circle fitting the desired square
     * @return Point belonging to a square (-halfSide to +halfSide)
     */
    fun squareFromAngle(radians: Double, halfSide: Double = 1.0): Vector2 {
        val q = HALF_PI * floor(4.0 * (radians + QUARTER_PI) / TAU % 4.0)
        val s = sin(q)
        val c = cos(q)
        val x = c * tan(radians) + s
        val y = s * tan(radians + q) - c
        return Vector2(x * halfSide, y * halfSide)
    }

    /**
     * Double.smoothstep(...)
     * Double.smootherstep(...)
     */
}