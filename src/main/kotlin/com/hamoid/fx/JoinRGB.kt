package com.hamoid.com.hamoid.fx

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.*
import org.openrndr.extra.color.presets.DARK_GRAY
import org.openrndr.extra.parameters.Description
import org.openrndr.extra.parameters.DoubleParameter
import org.openrndr.math.map
import org.openrndr.resourceUrl
    
/**
 * id: 231380fa-5b8c-4580-85ed-dce75f98a9f2
 * description: New sketch
 * tags: #new
 */    

@Description("Join RGB")
class JoinRGB : Filter(
    filterShaderFromUrl(resourceUrl("/shaders/join-rgb.frag"))
) {
    @DoubleParameter("Add", -1.0, 1.0, order = 10)
    var add: Double by parameters

    @DoubleParameter("Mult", 0.0, 1.0, order = 15)
    var mult: Double by parameters

    init {
        add = 0.0
        mult = 1.0
    }
}

/** Test JoinRGB */
fun main() = application {
    program {
        val rts = List(3) {
            renderTarget(width, height) {
                colorBuffer()
            }
        }
        rts.forEachIndexed { i, rt ->
            drawer.isolatedWithTarget(rt) {
                clear(ColorRGBa.BLACK)
                fill = ColorRGBa.WHITE
                strokeWeight = 8.0
                stroke = ColorRGBa.DARK_GRAY
                circle(
                    bounds.position(
                        i.toDouble().map(
                            0.0, 2.0,
                            0.25, 0.75),
                        0.5
                    ), 200.0
                )
            }
        }
        val result = colorBuffer(width, height)
        JoinRGB().apply(rts.map {
            it.colorBuffer(0)
        }.toTypedArray(), result)

        extend {
            drawer.image(result)
        }
    }
}
