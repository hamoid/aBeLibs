package com.hamoid.anim

import org.openrndr.events.Event
import org.openrndr.extra.easing.Easing
import org.openrndr.math.mix

/*
 * val x = Envelope2(100.0)
 * val y = Envelope2(
 *   values = listOf(50.0, 200.0),
 *   times = listOf(100, 1000),
 *   easings = listOf(Easing.QuadInOut),
 *   repetitions = 3)
 * repetitions = -1 for loop
 *
 * x.animateTo(300.0, dur_ms = 500, easing = Easing.QuadInOut)
 * x.append(400.0, dur_ms = 500, easing = Easing.QuadInOut)
 * x.delay(4000)
 * x.boomerang(1000, 100.0, 1000, easing = Easing.QuadInOut)
 * x.setEvents(listOf("end"))
 *
 * extend {
 *   x.tick()
 *   y.tick()
 *   println(x.toDouble())
 * }
 */
class Envelope2(private var currVal: Double = 0.0) {
    var paused = true
        private set

    private var posId = 0
    private val values = mutableListOf<Double>()
    private val times = mutableListOf<Int>()
    private val easings = mutableListOf<Easing>()
    private val eventArgs = mutableListOf<Any>(0)

    val done = Event<Any>()

    private var repetitions = 0
    private var time0 = 0L
    private var time1 = 0L
    private var val0 = 0.0
    private var val1 = 0.0
    private var easing = Easing.QuadInOut

    constructor(
        values: List<Double>,
        times: List<Int> = listOf(1000), // 1000ms transition
        easings: List<Easing> = listOf(Easing.QuadInOut),
        repetitions: Int = 1
    ) : this(values.first()) {
        initialize(values, times, easings, repetitions = repetitions)
    }

    companion object {
        fun tick(vararg envelopes: Envelope2) = envelopes.forEach { it.tick() }
    }

    fun initialize(
        values: List<Double>,
        times: List<Int> = listOf(1000),
        easings: List<Easing> = listOf(Easing.QuadInOut),
        repetitions: Int = 1,
        force: Boolean = false
    ) {
        if (!paused && !force) {
            // do nothing if still animating
            return
        }
        this.repetitions = repetitions
        this.values.clear()
        this.values.addAll(values)
        this.times.clear()
        this.times.addAll(times)
        this.easings.clear()
        this.easings.addAll(easings)
        paused = false
        posId = 0
        start()
    }

    private fun start() {
        time0 = System.currentTimeMillis()
        time1 = time0 + times[posId % times.size]
        val0 = values[posId % values.size]
        val1 = values[(posId + 1) % values.size]
        currVal = val0
        easing = easings[posId % easings.size]
    }

    /**
     * Animate towards targetVal in dur_ms
     * Skip dur_ms for instant teleporting.
     *
     * @param targetVal
     * @param durMs
     * @param easing
     */
    fun animateTo(
        targetVal: Double,
        durMs: Int = 1000,
        easing: Easing = Easing.QuadInOut
    ) {
        paused = true
        if (durMs > 0) {
            initialize(
                listOf(currVal, targetVal),
                listOf(durMs),
                listOf(easing)
            )
        } else {
            initialize(listOf(currVal))
        }
    }

    fun addDelay(delay: Int) {
        if (delay > 0) {
            append(values.last(), delay)
        }
    }

    /**
     * Append a new animation segment
     *
     * @param targetVal The desired new target value
     * @param durMs    The time it should take from the current
     * value to the target value.
     */
    fun append(
        targetVal: Double,
        durMs: Int,
        easing: Easing = Easing.QuadInOut
    ) {
        if (paused) {
            animateTo(targetVal, durMs)
        } else {
            values.add(targetVal)
            times.add(durMs)
            easings.add(easing)
        }
    }

    /**
     * Go from currVal to targetVal (in dur1_ms) and back to currVal (in dur2_ms)
     *
     * @param dur1Ms
     * @param targetVal
     * @param dur2Ms
     */
    fun boomerang(
        dur1Ms: Int, targetVal: Double,
        dur2Ms: Int,
        easing1: Easing = Easing.QuadInOut,
        easing2: Easing = easing1
    ) {
        if (paused) {
            initialize(
                listOf(currVal, targetVal, currVal),
                listOf(dur1Ms, dur2Ms),
                listOf(easing1, easing2)
            )
        } else {
            val lastVal = values.last()
            append(targetVal, dur1Ms, easing1)
            append(lastVal, dur2Ms, easing2)
        }
    }

    /**
     * Updates envelope values based on current time in milliseconds
     */
    fun tick() {
        if (paused) {
            return
        }

        val now = System.currentTimeMillis()
        var eventArg = -1
        if (now > time1) {
            if (eventArgs.isNotEmpty()) {
                eventArg = posId % eventArgs.size
            }
            posId++
            if ((posId + 1) % values.size == 0) {
                if (repetitions == 1) {
                    paused = true
                    if (eventArg >= 0) {
                        done.trigger(eventArgs[eventArg])
                    }
                    return
                } else {
                    repetitions--
                }
            }
            start()
        }
        val t = (now - time0) / (time1 - time0).toDouble()
        val eased = easing.function(t, 0.0, 1.0, 1.0)
        currVal = mix(val0, val1, eased)
        if (eventArg >= 0) {
            done.trigger(eventArgs[eventArg])
        }
    }

    fun setEvents(names: List<Any>) {
        eventArgs.clear()
        eventArgs.addAll(names)
    }

    override fun toString(): String {
        return """
            Envelope2:
                values: $values (posId $posId)
                times: $times
                easings: $easings
                eventArgs: $eventArgs
                repetitions: $repetitions
        """.trimIndent()
    }

    fun toDouble(): Double = currVal

    operator fun times(d: Double): Double = currVal * d
}

/*
enum class Test { ONE, TWO, THREE, FOUR }

fun main() = application {
    program {
        val env = Envelope2(100.0)
        env.initialize(
            listOf(400.0, 200.0, 400.0),
            listOf(2000, 500)
        )
        env.setEvents(listOf(Test.ONE, Test.TWO))
        env.done.listen {
            when (it) {
                Test.ONE -> println("UNO")
                Test.TWO -> {
                    println("DOS")
                    env.animateTo(0.0, 200, Easing.BounceOut)
                    env.setEvents(listOf(Test.THREE))
                }

                Test.THREE -> println("KOLME")
                Test.FOUR -> println("FOUR")
                else -> println("Unknown event")
            }
        }
        extend {
            env.tick()
            drawer.circle(env.toDouble(), 100.0, 50.0)
        }
        keyboard.keyDown.listen {
            if (it.key == KEY_ENTER) {
                env.animateTo(Random.double0(width * 1.0), 200)
                env.setEvents(listOf(Test.FOUR))
            }
        }
    }
}
*/
