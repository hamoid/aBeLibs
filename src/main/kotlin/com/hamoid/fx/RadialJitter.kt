package com.hamoid.com.hamoid.fx

import org.openrndr.application
import org.openrndr.draw.*
import org.openrndr.extra.parameters.Description
import org.openrndr.extra.parameters.DoubleParameter
import org.openrndr.resourceUrl
    
/**
 * id: fe202122-3dc9-47df-a287-2a12c8649322
 * description: New sketch
 * tags: #new
 */    

/**
 * Radial jitter
 *
 * Creates a radial gradient that is black at the center
 * and has random increasing green and red amounts when
 * moving away from the center
 */
@Description("Radial Jitter")
class RadialJitter : Filter(
    filterShaderFromUrl(resourceUrl("/shaders/radial-jitter.frag"))
) {

    @DoubleParameter("Minumum blur", 0.0, 5.0, order = 10)
    var minBlur: Double by parameters

    @DoubleParameter("Minimum distance", 0.0, 5.0, order = 15)
    var minDist: Double by parameters

    @DoubleParameter("Maximum distance", 0.0, 5.0, order = 20)
    var maxDist: Double by parameters

    @DoubleParameter("Maximum blur", 0.0, 5.0, order = 25)
    var maxBlur: Double by parameters

    @DoubleParameter("Noise amount", 0.0, 5.0, order = 30)
    var noiseAmount: Double by parameters

    init {
        minDist = 0.3
        maxDist = 0.4
        minBlur = 0.1
        maxBlur = 1.0
        noiseAmount = 0.2
    }
}

/** Test */
fun main() = application {
    program {
        val radialJitter = RadialJitter()
        val result = colorBuffer(width, height, type = ColorType.FLOAT32)
        extend {
            radialJitter.apply(emptyArray(), result)
            drawer.image(result)
        }
    }
}
