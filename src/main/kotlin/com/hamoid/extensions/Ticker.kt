package com.hamoid.com.hamoid.extensions

import org.openrndr.Extension
import org.openrndr.Program
import org.openrndr.draw.Drawer

class Ticker(val interval: Double) : Extension {
    override var enabled: Boolean = true

    var tick = false
    private var previousTime = 0

    override fun beforeDraw(drawer: Drawer, program: Program) {
        val currentTime = (program.application.seconds / interval).toInt()
        tick = previousTime != currentTime
        previousTime = currentTime
    }
}

//fun main() = application {
//    val ticker = Ticker(0.5)
//    program {
//        extend(ticker)
//        extend {
//            drawer.circle(drawer.bounds.center, if(ticker.tick) 100.0 else 50.0)
//        }
//    }
//}