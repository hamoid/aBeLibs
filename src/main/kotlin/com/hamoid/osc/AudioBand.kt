package com.hamoid.osc

/**
 * Copied from old aBeLive
  */
class AudioBand(
    var valRecv: Double = 0.0,
    var valCurr: Double = 0.0,
    var valLast: Double = 0.0,
    var delta: Double = 0.0
) {
    companion object {
        // Limit the number of onsets
        private const val onsetRateLimitMs = 1000
    }

    private var nextOnset = 0L

    fun update() {
        valLast = valCurr
        valCurr = 0.1 * valCurr + 0.9 * valRecv
        delta = valCurr - valLast
    }

    fun onset(threshold: Double): Boolean {
        val now = System.currentTimeMillis()
        if (now > nextOnset) {
            val invThreshold = 1 - threshold
            if (delta > invThreshold * invThreshold) {
                nextOnset = now + onsetRateLimitMs
                return true
            }
        }
        return false
    }
}
