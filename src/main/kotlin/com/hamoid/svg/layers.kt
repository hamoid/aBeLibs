package com.hamoid.svg

import org.openrndr.extra.composition.*
import org.openrndr.extra.svg.toSVG
import org.openrndr.math.IntVector2
import java.io.File

fun compositionSizeA(n: Int, portrait: Boolean = true): CompositionDimensions {
    val size = when (n) {
        -1 -> IntVector2(1682, 2378)
        -2 -> IntVector2(1189, 1682)
        0 -> IntVector2(841, 1189)
        1 -> IntVector2(594, 841)
        2 -> IntVector2(420, 594)
        3 -> IntVector2(297, 420)
        4 -> IntVector2(210, 297)
        5 -> IntVector2(148, 210)
        6 -> IntVector2(105, 148)
        7 -> IntVector2(74, 105)
        8 -> IntVector2(52, 74)
        9 -> IntVector2(37, 52)
        else -> IntVector2(26, 37)
    }
    val actualSize = (if (portrait) size else size.yx).vector2
    return CompositionDimensions(
        0.0.pixels, 0.0.pixels,
        Length.Pixels.fromMillimeters(actualSize.x),
        Length.Pixels.fromMillimeters(actualSize.y)
    )
}

fun GroupNode.setInkscapeLayer(name: String) {
    attributes["inkscape:groupmode"] = "layer"
    attributes["inkscape:label"] = name
}

fun Composition.saveToInkscapeFile(file: File) {
    namespaces["xmlns:inkscape"] = "http://www.inkscape.org/namespaces/inkscape"
    namespaces["xmlns:sodipodi"] = "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
    namespaces["xmlns:svg"] = "http://www.w3.org/2000/svg"

    val svg = StringBuilder(toSVG())

    val header = """
        <sodipodi:namedview
            id="namedview7112"
            pagecolor="#ffffff"
            bordercolor="#eeeeee"
            borderopacity="1"
            inkscape:showpageshadow="0"
            inkscape:pageopacity="0"
            inkscape:pagecheckerboard="0"
            inkscape:deskcolor="#d1d1d1"
            showgrid="false"
            inkscape:zoom="1.0"
            inkscape:cx="${bounds.width.value / 2}"
            inkscape:cy="${bounds.height.value / 2}"
            inkscape:window-width="${bounds.width}"
            inkscape:window-height="${bounds.height}"
            inkscape:window-x="0"
            inkscape:window-y="0"
            inkscape:window-maximized="1"
            inkscape:current-layer="openrndr-svg" />        
    """.trimIndent()

    // Remove the wrapping <g>, otherwise layers don't work.
    val updated = svg.replace(
        Regex("""(<g\s?>(.*)</g>)""", RegexOption.DOT_MATCHES_ALL), "$2"
    ).replace(
        Regex("""(<svg.*?>)""", RegexOption.DOT_MATCHES_ALL), "$1$header"
    )
    file.writeText(updated)
}
