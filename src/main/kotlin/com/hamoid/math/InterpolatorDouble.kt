package com.hamoid.math

import com.hamoid.com.hamoid.math.InterpolatorConfig
import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extra.noise.Random
import org.openrndr.math.Polar
import org.openrndr.math.clamp
import org.openrndr.math.map
import kotlin.math.abs
import kotlin.math.min
import kotlin.math.sign
    
/**
 * id: 1dead2c9-1ac5-4c8f-ae01-67bd8c697a15
 * description: New sketch
 * tags: #new
 */    

/**
 * Smoothly interpolate a [Double] towards [target]. It uses
 * [velocity] which provides acceleration and deceleration.
 * You can specify a [wrap] value of 360.0 when working with
 * angles, so rotation happens taking the shortest path:
 * 350° and 10° are only 20° apart, not 340°.
 */
class InterpolatorDouble(pos: Double, private val wrap: Double = 0.0) {
    /**
     * Current value, but call [update] first on every animation frame
     */
    var current = pos
        private set

    /**
     * The [target] can be set at any time. Velocity will
     * smoothly change to reach that [target].
     */
    var target = current
        set(value) {
            var targetValue = value
            if (wrap != 0.0) {
                targetValue = targetValue.mod(wrap)
                current = current.mod(wrap)
                if (abs(targetValue - current) > wrap / 2.0) {
                    if (targetValue > current) current += wrap else targetValue += wrap
                }
            }
            if (field != targetValue) paused = false
            field = targetValue
        }

    var conf = InterpolatorConfig.screenMedium()

    /**
     * Optimization. Stop calculating when distance and velocity
     * are below [epsilon] = destination reached.
     */
    var paused = false
        private set

    var velocity = 0.0
        private set

    /**
     * Update simulation and get new value
     */
    fun update() {
        if (paused) return

        var offset = target - current
        val len = abs(offset)
        val dist = min(conf.dampDistance, len) // 0.0 .. dampDist

        // convert dist to desired speed
        offset = offset.sign *
                dist.map(0.0, conf.dampDistance, 0.0, conf.maxSpeed)

        val acceleration = clamp(
            offset - velocity,
            -conf.maxAcceleration, conf.maxAcceleration
        )

        velocity = clamp(
            velocity + acceleration,
            -conf.maxSpeed, conf.maxSpeed
        )

        current += velocity

        //println("$len ${abs(velocity)} $epsilon")
        if (len < conf.epsilon && abs(velocity) < conf.epsilon) {
            paused = true
        }
    }

    /**
     * Teleport to [target] zeroing the current velocity
     */
    @Suppress("unused")
    fun jumpTo(target: Double) {
        velocity = 0.0
        current = target
        this.target = target
    }

    /**
     * Makes it possible to use the [InterpolatorDouble] in a numerical
     * expression without having to use [current].
     */
    operator fun times(v: Number) = current * v.toDouble()
}

fun main() = application {
    program {
        val i = InterpolatorDouble(0.5)
        i.conf.setup(0.05, 0.0005, 0.5, 0.0001)

        val a = InterpolatorDouble(180.0, 360.0)
        a.conf.setup(15.0, 0.2, 300.0)

        extend {
            i.update()
            a.update()

            val cy = height * i.current.map(0.1, 0.9)
            val ty = height * i.target.map(0.1, 0.9)

            drawer.stroke = ColorRGBa.PINK
            drawer.lineSegment(0.0, ty, width * 1.0, ty)

            drawer.stroke = if (i.paused) ColorRGBa.GRAY else ColorRGBa.WHITE
            drawer.lineSegment(0.0, cy, width * 1.0, cy)

            drawer.lineSegment(
                Polar(a.current, 100.0).cartesian + drawer.bounds.center, drawer.bounds.center
            )
        }
        mouse.buttonDown.listen {
            i.target = if (i.target > 0.5) 0.0 else 1.0
            a.target = Random.int0(36) * 30.0
        }
    }
}
