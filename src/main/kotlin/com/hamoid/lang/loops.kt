package com.hamoid.lang

inline fun doubleRepeat(steps: Int, from: Double = 0.0, to: Double = 1.0,
                        action: (Double) -> Unit
) {
    for (index in 0 until steps) {
        action(from + (to - from) * index / (steps - 1))
    }
}

inline fun loopRepeat(steps: Int, from: Double = 0.0, to: Double = 1.0,
                      action: (Double) -> Unit
) {
    for (index in 0 until steps) {
        action(from + (to - from) * index / steps)
    }
}

inline fun doubleFor(from: Double, to: Double, step: Double,
                        action: (Double) -> Unit) {
    var v = from
    while (v < to) {
        action(v)
        v += step
    }
}

infix fun ClosedRange<Double>.step(step: Double): Iterable<Double> {
    require(start.isFinite())
    require(endInclusive.isFinite())
    require(step > 0.0) { "Step must be positive, was: $step." }
    val sequence = generateSequence(start) { previous ->
        if (previous == Double.POSITIVE_INFINITY) return@generateSequence null
        val next = previous + step
        if (next > endInclusive) null else next
    }
    return sequence.asIterable()
}
