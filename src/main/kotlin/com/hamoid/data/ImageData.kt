package com.hamoid.data

import com.hamoid.random.file
import org.openrndr.boofcv.binding.resizeTo
import org.openrndr.draw.ColorBuffer
import org.openrndr.draw.ColorBufferShadow
import org.openrndr.draw.loadImage
import org.openrndr.extra.noise.Random
import org.openrndr.extra.noise.shapes.uniform
import org.openrndr.math.Vector2
import org.openrndr.shape.IntRectangle
import org.openrndr.shape.Rectangle

/**
 * Loads a random image from a folder and lets the user
 * read the pixels from that image.
 */
data class ImageData(
    val path: String,
    val scanDist: Double = 8.0,
    val moveDist: Double = 3.0,
    val ext: String = "jpg",
    val width: Int = 900,
    val height: Int = 900
) {
    /**
     * GPU buffer containing the image
     */
    lateinit var buff: ColorBuffer

    /**
     * pixels of the image available for reading
     */
    lateinit var pixels: ColorBufferShadow

    /**
     * Area equal to the image bounds minus `scanDist`
     */
    lateinit var safeArea: Rectangle

    /**
     * Load nextand resize to 900x900
     *
     */
    fun loadNext() {
        Random.unseeded {
            buff = loadImage(Random.file(path, ext)).resizeTo(width, height)
        }
        setup()
    }

    /**
     * Load next specifying a crop area from the image
     */
    fun loadNext(crop: IntRectangle) {
        Random.unseeded {
            val loaded = loadImage(Random.file(path, ext))
            val shiftedCrop = crop.copy(
                (loaded.bounds - crop.rectangle).uniform().toInt()
            )
            buff = loaded.crop(shiftedCrop)
        }
        setup()
    }

    private fun setup() {
        pixels = buff.shadow
        pixels.download()
        safeArea = buff.bounds.offsetEdges(scanDist * -2.0)
    }

    fun getColor(normPos: Vector2) = pixels[
            (normPos.x * buff.width).toInt().coerceIn(0 until buff.width),
            (normPos.y * buff.height).toInt().coerceIn(0 until buff.height)
    ]

    init {
        loadNext()
    }
}