package com.hamoid.color

import org.openrndr.draw.ColorBuffer
import org.openrndr.draw.Drawer
import org.openrndr.draw.MagnifyingFilter
import org.openrndr.ffmpeg.PlayMode
import org.openrndr.ffmpeg.VideoPlayerFFMPEG
import java.io.File
import java.io.FileFilter

/**
 * Provides a texture based on a playing video file.
 */
class VideoTextures(folder: String, private var id: Int = 0) {
    private val files = File(folder).listFiles(FileFilter {
        it.extension.lowercase() == "mp4"
    })!!.sorted()

    private fun load() {
        println("Play $id: ${files[id].absolutePath}")
        video = VideoPlayerFFMPEG.fromFile(
            files[id].absolutePath,
            PlayMode.VIDEO
        )
        video.ended.listen { video.restart() }
        video.play()
    }

    private lateinit var video: VideoPlayerFFMPEG

    init { load() }

    /**
     * Update the video frame and draw the content into [target]
     */
    fun update(drawer: Drawer, target: ColorBuffer) {
        video.draw(drawer, blind = true)
        video.colorBuffer?.let {
            it.copyTo(
                target, 0, 0,
                it.bounds.toInt(),
                target.bounds.flippedVertically().toInt(),
                MagnifyingFilter.NEAREST
            )
        }
    }

    fun seek(t: Double) {
        video.seek(t * video.duration)
    }

    fun next() {
        id = (id + 1) % files.size
        video.ended.listeners.clear()
        video.dispose()
        load()
    }
}
