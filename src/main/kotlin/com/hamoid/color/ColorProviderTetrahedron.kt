package com.hamoid.color

import org.openrndr.color.ColorLABa
import org.openrndr.color.ColorRGBa
import org.openrndr.math.Vector3

private inline val Double.coerce01: Double
    get() = coerceIn(0.0, 1.0)

/**
 * A Tetrahedron ColorProvider with a color on each of its 4 vertices.
 * A color can be sampled via one of its get methods by passing a
 * Vector3 to specify a point in the volume. The 4 vertex colors are
 * interpolated linearly to return a mixed color. The mixing
 * happens either in LABa or RGBa color spaces.
 */
class ColorProviderTetrahedron(
    private val colors: List<ColorLABa>,
    val name: String
) {

    /**
     * Constructor taking 4 hex RGB strings and a [name]
     */
    constructor(a: String, b: String, c: String, d: String, name: String) : this(
        listOf(
            ColorRGBa.fromHex(a).toLABa(),
            ColorRGBa.fromHex(b).toLABa(),
            ColorRGBa.fromHex(c).toLABa(),
            ColorRGBa.fromHex(d).toLABa()
        ), name
    )

    /**
     * Get a color using [p] to specify the XY location on a
     * tetrahedron base triangle and the Z elevation towards its cusp.
     * Uses LABa mixing.
     */
    fun getColor(p: Vector3): ColorRGBa {
        if (colors.size != 4) {
            return ColorRGBa.BLACK
        }
        val a = colors[0].mix(colors[1], p.x.coerce01)
        val b = a.mix(colors[2], p.y.coerce01)
        return b.mix(colors[3], p.z.coerce01).toRGBa()
    }

    /**
     * Get a color using [p] to specify the XY location on a
     * tetrahedron (pyramid) and the Z elevation towards its cusp.
     * Uses RGBa mixing.
     */
    fun getColorViaRGB(p: Vector3): ColorRGBa {
        if (colors.size != 4) {
            return ColorRGBa.BLACK
        }
        val a = colors[0].toRGBa().mix(colors[1].toRGBa(), p.x.coerce01)
        val b = a.mix(colors[2].toRGBa(), p.y.coerce01)
        return b.mix(colors[3].toRGBa(), p.z.coerce01)
    }

    override fun toString() = """
        ColorProviderTetrahedron($name)
        $colors
    """.trimIndent()
}
