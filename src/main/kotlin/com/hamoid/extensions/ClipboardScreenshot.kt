package com.hamoid.extensions

import org.openrndr.Extension
import org.openrndr.KeyModifier
import org.openrndr.Program
import org.openrndr.extensions.Screenshots

/**
 * OPENRNDR extension to enable
 * CTRL+C to copy the visible content to the clipboard on Linux or
 * other OSes providing the `xclip` command line program.
 *
 * Usage: `extend(ClipboardScreenshot())`
 * then press CTRL+C to copy what you see, paste the image somewhere.
 */

class ClipboardScreenshot : Extension {
    override var enabled: Boolean = true

    private val screenshots = Screenshots().also {
        it.listenToKeyDownEvent = false
        it.listenToProduceAssetsEvent = false
        it.name = "/tmp/openrndr-screenshot.png"
        it.async = false
        it.afterScreenshot.listen { _ ->
            Runtime.getRuntime().exec(
                arrayOf(
                    "xclip",
                    "-selection", "clipboard",
                    "-t", "image/png",
                    it.name
                )
            )
        }
    }

    override fun setup(program: Program) {
        program.extend(screenshots)
        println("Listening to CTRL+C")
        program.keyboard.keyDown.listen {
            if (KeyModifier.CTRL in it.modifiers &&
                it.name == "c"
            ) screenshots.trigger()
        }
    }
}
