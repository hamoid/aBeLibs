package com.hamoid.com.hamoid.extensions

import com.hamoid.math.InterpolatorDouble
import org.openrndr.Extension
import org.openrndr.Program
import org.openrndr.draw.Drawer
import org.openrndr.events.Event
import org.openrndr.extra.midi.MidiDeviceDescription
import org.openrndr.extra.midi.MidiTransceiver

/**
 * Midi vortex2 extension.
 * Usage: `extend(MidiVortex2())`
 * I can receive NOTE_ON, CONTROL_CHANGED, PITCH_BEND, PRESSURE
 */
class MidiVortex2Minimal(program: Program) : Extension {
    override var enabled: Boolean = true

    // Sliding
    var ribbons = Array(3) { InterpolatorDouble(0.0) }
    val pads = Array(8) { InterpolatorDouble(0.0) }
    var tilt = InterpolatorDouble(0.0)
    var sideSlider = InterpolatorDouble(0.0)
    var bend = InterpolatorDouble(0.0)
    var pressure = InterpolatorDouble(0.0)

    // Discrete
    val sliders = Array(8) { InterpolatorDouble(0.0) }
    val notes = Array(127) { InterpolatorDouble(0.0) }
    var sustain = InterpolatorDouble(0.0)

    var txt = ""

    /**
     * [controller] holds the open MIDI device or null if it can't be found.
     */
    private val controller = try {
        MidiDeviceDescription.list().forEach(::println)
        MidiDeviceDescription.list().firstOrNull { it.name.contains("V2") }
            ?.run {
                MidiTransceiver.fromDeviceVendor(program, name, vendor)
            }
    } catch (e: IllegalArgumentException) {
        null
    }

    /**
     * Events sent by notes and pads when pressed / released. They can be used
     * to create things that get added to a collection, then told to die on
     * release.
     */
    val noteEvents = Event<Pair<Int, InterpolatorDouble?>>("V2Notes", true)
    val padEvents = Event<Pair<Int, InterpolatorDouble?>>("V2Pads", true)

    init {
        controller?.noteOn?.run {
            postpone = true
            listen {
                val v = it.velocity / 127.0
                when (it.channel) {
                    0 -> notes[it.note].let { note ->
                        note.target = v
                        noteEvents.trigger(Pair(it.note, note))
                    }

                    8 -> pads[it.note - 48].let { pad ->
                        pad.target = v
                        padEvents.trigger(Pair(it.note - 48, pad))
                    }

                    else -> txt = it.toString()
                }
            }
        }
        controller?.noteOff?.run {
            postpone = true
            listen {
                when (it.channel) {
                    0 -> notes[it.note].let { note ->
                        note.target = 0.0
                        noteEvents.trigger(Pair(it.note, note))
                    }

                    8 -> pads[it.note - 48].let { pad ->
                        pad.target = 0.0
                        padEvents.trigger(Pair(it.note - 48, pad))
                    }

                    else -> txt = it.toString()
                }
            }
        }
        controller?.controlChanged?.run {
            postpone = true
            listen {
                val v = it.value / 127.0
                when (it.control) {
                    91 -> tilt.target = v
                    7 -> sideSlider.target = v
                    in 100..102 -> ribbons[it.control - 100].target = v
                    64 -> sustain.target = v
                    in 36..43 -> sliders[it.control - 36].target = v
                    else -> txt = it.toString()
                }
            }
        }

        controller?.pitchBend?.run {
            postpone = true
            listen {
                bend.target = it.pitchBend / 127.0 // equals ribbon 2
            }
        }

        controller?.channelPressure?.run {
            postpone = true
            listen {
                pressure.target = it.pressure / 127.0
            }
        }
    }

    /**
     * Deliver events and update all interpolators
     */
    override fun beforeDraw(drawer: Drawer, program: Program) {
        controller?.run {
            ribbons.forEach(InterpolatorDouble::update)
            sliders.forEach(InterpolatorDouble::update)
            notes.forEach(InterpolatorDouble::update)
            pads.forEach(InterpolatorDouble::update)
            tilt.update()
            sideSlider.update()
            bend.update()
            pressure.update()
            sustain.update()

            noteOn.deliver()
            noteOff.deliver()
            controlChanged.deliver()
            pitchBend.deliver()
            channelPressure.deliver()

            noteEvents.deliver()
            padEvents.deliver()
        }
    }
}
