package com.hamoid.svg

import com.hamoid.geometry.separated
import com.hamoid.math.map
import org.openrndr.extra.composition.Composition
import org.openrndr.extra.composition.draw
import org.openrndr.extra.composition.CompositionDrawer
import org.openrndr.extra.composition.drawComposition
import org.openrndr.extra.composition.ClipMode
import org.openrndr.extra.noise.Random
import org.openrndr.extra.noise.poissonDiskSampling
import org.openrndr.extra.shapes.hobbycurve.hobbyCurve
import org.openrndr.math.Polar
import org.openrndr.math.Vector2
import org.openrndr.shape.Circle
import org.openrndr.shape.LineSegment
import org.openrndr.shape.Shape
import org.openrndr.shape.ShapeContour
import kotlin.math.pow

class Pattern {
    companion object {
        var stroke: Boolean = false
    }

    data class STRIPES(
        val expo: Double = 1.0,
        val density: Double = 1.0,
        val rotation: Double = 0.0
    )

    data class NOISE(
        val expo: Double = 1.0,
        val density: Double = 1.0,
        val rotation: Double = 0.0,
        val noiseDisplacement: Double = 10.0,
        val noiseZoom: Double = 0.1
    )

    data class CIRCLES(
        val expo: Double = 1.0,
        val center: Vector2,
        val density: Double = 1.0
    )

    data class HAIR(
        val length: Double = 2.0,
        val zoom: Double = 1.0,
        val separation: Double = 1.0
    )

    data class PERP(
        val length: Double = 2.0,
        val separation: Double = 1.0
    )

    data class DOTS(
        val fillPercent: Double = 0.001,
        val minSize: Double = 5.0,
        val maxSize: Double = 20.0,
        val sizeSkew: Double = 1.0,
        val separation: Double = 5.0
    )
}

fun Composition.fill(outline: Shape, patternCfg: Any) {
    var pattern: Composition? = null
    val radius = outline.bounds.dimensions.length / 2
    val off = outline.bounds.center
    this.draw {
        when (patternCfg) {

            is Pattern.STRIPES -> {
                val num = (radius * patternCfg.density).toInt()
                val rot = patternCfg.rotation
                pattern = outline.addPattern {
                    lineSegments(List(num) { segNum ->
                        val yNorm = (segNum / (num - 1.0)).pow(patternCfg.expo)
                        val x = ((segNum % 2) * 2 - 1.0) * radius
                        val y = (yNorm * 2 - 1) * radius
                        val start = Vector2(-x, y).rotate(rot) + off
                        val end = Vector2(x, y).rotate(rot) + off
                        LineSegment(start, end)
                    })
                }
            }

            is Pattern.NOISE -> {
                val safeRadius = radius + patternCfg.noiseDisplacement
                val num = (safeRadius * patternCfg.density).toInt()
                val rot = patternCfg.rotation
                pattern = outline.addPattern {
                    val c = List(num) { segNum ->
                        val yNorm = (segNum / (num - 1.0)).pow(patternCfg.expo)
                        val y = (yNorm * 2 - 1) * safeRadius
                        val start = Vector2(-safeRadius, y).rotate(rot) + off
                        val end = Vector2(safeRadius, y).rotate(rot) + off
                        val count = 10
                        ShapeContour.fromPoints(List(count) {
                            val pc = it / (count - 1.0)
                            val base = start.mix(end, pc)
                            base + patternCfg.noiseDisplacement *
                                    Random.simplex(base * patternCfg.noiseZoom)
                        }, false).hobbyCurve() // requires orx-shapes
                    }
                    contours(c)
                }
            }

            is Pattern.CIRCLES -> {
                val num = (radius * patternCfg.density).toInt()
                pattern = outline.addPattern {
                    contours(List(num) { segNum ->
                        val t = (segNum / (num - 1.0)).pow(patternCfg.expo)
                        Circle(patternCfg.center + off, 1 + 2 * radius * t)
                            .contour.open
                    })
                }
            }

            is Pattern.HAIR -> {
                val positions = poissonDiskSampling(
                    outline.bounds,
                    patternCfg.separation, 20
                ) //{ p -> p in outline }
                pattern = outline.addPattern {
                    lineSegments(positions.map {
                        val offset = Polar(
                            360 * Random.simplex(
                                it * patternCfg.zoom +
                                        outline.bounds.corner * 0.1
                            ),
                            patternCfg.length / 2.0
                        ).cartesian
                        LineSegment(it - offset, it + offset)
                    })
                }
            }

            is Pattern.PERP -> {
                val positions = poissonDiskSampling(
                    outline.bounds,
                    patternCfg.separation, 20
                ) //{ p -> p in outline }

                pattern = outline.addPattern {
                    val contour = outline.contours.first()
                    lineSegments(positions.map {
                        val offset = (contour.nearest(it).position - it)
                            .normalized * patternCfg.length / 2.0
                        LineSegment(it - offset, it + offset)
                    })
                }
            }

            is Pattern.DOTS -> {
                // openrndr 0.4 doesn't provide .area yet
                //val count = 2 + (outline.area * patternCfg.fillPercent).toInt()
                val count = 2 + (outline.bounds.width * outline.bounds.height
                        * 0.79 * patternCfg.fillPercent).toInt()
                var positions = (Random.ring2d(0.0, radius, count) as
                        List<Vector2>).map {
                    val rad = Random.double0().pow(patternCfg.sizeSkew).map(
                        patternCfg.minSize, patternCfg.maxSize
                    )
                    Circle(it + off, rad)
                }
                for (i in 0..100) {
                    positions = positions.separated(
                        patternCfg.separation,
                        outline.contours[0]
                    )
                }
                pattern = outline.addPattern {
                    contours(positions.map { it.contour.open })
                }
            }
        }
        pattern?.apply {
            composition(this)
        }
        if (Pattern.stroke) {
            shape(outline)
        }
    }
}

/**
 * Takes a function that draws a pattern and discards everything
 * that is outside the "cookiecutter" Shape.
 *
 * @return A new composition including only the intersecting parts
 */
fun Shape.addPattern(pattern: CompositionDrawer.() -> Unit): Composition {
    val cutter = this
    // Clip pattern using the cutter shape
    return drawComposition {
        pattern()
        clipMode = ClipMode.INTERSECT
        shape(cutter)
    }
}
