rootProject.name = "aBeLibs"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenLocal()
    }
}
