package com.hamoid.compute

import org.openrndr.extra.shaderphrases.ShaderPhrase
import org.openrndr.extra.shaderphrases.ShaderPhraseBook

/**
 * Usage: `ShaderPhrasesCS.register()`
 *
 * ```
 *     val glsl = """
 *         foo
 *
 *         #pragma import bookCS.random
 *         #pragma import bookCS.random2
 *         #pragma import bookCS.limit
 *         #pragma import bookCS.randomDir
 *         #pragma import bookCS.rotation2d
 *
 *         bar
 *     """.trimIndent()
 *
 *     println(preprocessShader(glsl))
 * ```
 */
class ShaderPhrasesCS : ShaderPhraseBook("bookCS") {

    /**
     * Returns normalized float.
     *
     * GLSL: `float random(vec2 v)`
     */
    val random = ShaderPhrase(
        """
            float random(vec2 co) {
                float a = 12.9898;
                float b = 78.233;
                float c = 43758.5453;
                float dt = dot(co.xy, vec2(a, b));
                float sn = mod(dt, 3.14);
                return fract(sin(sn) * c);
            }  
            """.trimIndent()
    )

    /**
     * Returns vec2 between 0.0 and 1.0.
     *
     * GLSL: `vec2 random2(vec2 v)`
     */
    val random2 = ShaderPhrase(
        """
            vec2 random2(vec2 p) {
                vec3 a = fract(p.xyx + vec3(123.34, 234.34, 345.65));
                a += dot(a, a + 34.45);
                return fract(vec2(a.x * a.y, a.y * a.z));
            }                
            """.trimIndent()
    )

    /**
     * Returns a vec2 with length between 0.0 and `max`.
     *
     * GLSL: `vec2 limit(vec2 v, float max)`
     */
    val limit = ShaderPhrase(
        """
            vec2 limit(vec2 v, float max) {
                if(dot(v, v) <= max * max) {
                    return v;
                } else {
                    return normalize(v) * max;
                }
            }                    
            """.trimIndent()
    )

    /**
     * Returns a normalized random vec2.
     *
     * GLSL: `vec2 randomDir(vec2 seed)`
     */
    val randomDir = ShaderPhrase(
        """
            vec2 randomDir(vec2 p) {
                return normalize(2.0 * (random2(p) - 0.5));
            }
            """.trimIndent()
    )

    /**
     * Returns a 2D rotation `mat2`.
     *
     * GLSL: `mat2 rotation2d(float radians)`
     */
    val rotation2d = ShaderPhrase(
        """
            mat2 rotation2d(float angle) {
              float s = sin(angle);
              float c = cos(angle);
              return mat2(c, -s, s, c);
            }
            """.trimIndent()
    )

    companion object {
        fun register() = ShaderPhrasesCS().register()
    }
}
