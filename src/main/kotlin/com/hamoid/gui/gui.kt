package com.hamoid.gui

import org.openrndr.color.rgb
import org.openrndr.extra.gui.GUIAppearance
import org.openrndr.panel.style.Color
import org.openrndr.panel.style.defaultStyles

fun GUI(
    baseColor: String,
    controlBackground: String,
    controlHoverBackground: String,
    controlTextColor: String,
    controlActiveColor: String,
    controlFontSize: Double = 14.0
) = org.openrndr.extra.gui.GUI(
    GUIAppearance(rgb(baseColor)),
    defaultStyles(
        rgb(controlBackground),
        rgb(controlHoverBackground),
        Color.RGBa(rgb(controlTextColor)),
        Color.RGBa(rgb(controlActiveColor)),
        controlFontSize
    )
)
