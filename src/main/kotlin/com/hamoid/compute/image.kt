package com.hamoid.compute

import org.openrndr.color.ColorRGBa
import org.openrndr.draw.*
import org.openrndr.math.IntVector2
import org.openrndr.shape.IntRectangle
import org.openrndr.shape.Rectangle

/**
 * We call this method each time before `execute()`.
 *
 * This is an alternative implementation of [ComputeShader.image] which
 * takes 2 arguments instead of 4. This is possible because
 * [TextureBinding] contains name, texture and access type.
 */
fun ComputeShader.bindImage(texBinding: TextureBinding, image: Int) {
    image(
        texBinding.name,
        image,
        texBinding.namedTexture.bind(texBinding.access)
    )
}

/**
 * Simple data class with a [ColorBuffer] and a name.
 *
 */
open class NamedTexture(var tex: ColorBuffer, val name: String) {
    fun swap(other: NamedTexture) {
        this.tex = other.tex.also {
            other.tex = this.tex
        }
    }

    val size get() = IntVector2(tex.width, tex.height)

    fun bind(access: ImageAccess, level: Int = 0) =
        tex.imageBinding(level, access)

    fun clear(): NamedTexture {
        tex.fill(ColorRGBa.TRANSPARENT)
        return this
    }

    fun binding(access: ImageAccess) = TextureBinding(this, access)

    companion object {
        /**
         * Creates a [NamedTexture] with a [ColorBuffer] and a [name]
         */
        fun create(
            rez: Int,
            colorFormat: ColorFormat = ColorFormat.RGBa,
            colorType: ColorType = ColorType.FLOAT32,
            name: String
        ) = create(rez, rez, colorFormat, colorType, name)

        fun create(
            rez: IntVector2,
            colorFormat: ColorFormat = ColorFormat.RGBa,
            colorType: ColorType = ColorType.FLOAT32,
            name: String
        ) = create(rez.x, rez.y, colorFormat, colorType, name)

        fun create(
            width: Int, height: Int,
            colorFormat: ColorFormat = ColorFormat.RGBa,
            colorType: ColorType = ColorType.FLOAT32,
            name: String
        ) = NamedTexture(
            colorBuffer(width, height, 1.0, colorFormat, colorType).apply {
                wrapV = WrapMode.REPEAT
                wrapU = WrapMode.REPEAT
                filterMin = MinifyingFilter.NEAREST
                filterMag = MagnifyingFilter.NEAREST
            }, name
        )

        fun load(
            imagePath: String,
            width: Int, height: Int,
            colorFormat: ColorFormat = ColorFormat.RGBa,
            colorType: ColorType = ColorType.FLOAT32,
            name: String
        ): NamedTexture {
            val img = loadImage(imagePath)
            val cb = colorBuffer(
                width, height, 1.0, colorFormat, colorType
            ).apply {
                wrapV = WrapMode.REPEAT
                wrapU = WrapMode.REPEAT
                filterMin = MinifyingFilter.LINEAR
                filterMag = MagnifyingFilter.LINEAR
            }
            img.copyTo(
                cb,
                sourceRectangle = IntRectangle(0, 0, img.width, img.height),
                targetRectangle = IntRectangle(0, 0, width, height)
            )
            return NamedTexture(cb, name)
        }

    }
}

/**
 * A binding between a [NamedTexture] and an [ImageAccess]
 */
data class TextureBinding(
    val namedTexture: NamedTexture,
    val access: ImageAccess
) {
    /**
     * The layout() code to include at the top of the compute shader
     */
    val glslLayout: String
        get() {
            val tex = namedTexture.tex
            val layout = imageLayout(tex.format, tex.type)
            val modeString = when (access) {
                ImageAccess.READ -> "readonly"
                ImageAccess.WRITE -> "writeonly"
                else -> ""
            }
            return "layout($layout) uniform $modeString image2D $name;"
        }


    val name get() = namedTexture.name
}

/**
 * Draws a [NamedTexture] using a [Drawer]. The target size can be specified
 * using [bounds].
 */
fun Drawer.image(
    nTex: NamedTexture,
    bounds: Rectangle = this.bounds
) = image(nTex.tex, nTex.tex.bounds, bounds)

/**
 * From openrndr/openrndr-gl-common/src/commonMain/kotlin/Mapping.kt
 */
private fun imageLayout(format: ColorFormat, type: ColorType) =
    when (Pair(format, type)) {
        Pair(ColorFormat.R, ColorType.UINT8) -> "r8"
        Pair(ColorFormat.R, ColorType.UINT8_INT) -> "r8u"
        Pair(ColorFormat.R, ColorType.SINT8_INT) -> "r8i"
        Pair(ColorFormat.R, ColorType.UINT16) -> "r16"
        Pair(ColorFormat.R, ColorType.UINT16_INT) -> "r16u"
        Pair(ColorFormat.R, ColorType.SINT16_INT) -> "r16i"
        Pair(ColorFormat.R, ColorType.FLOAT16) -> "r16f"
        Pair(ColorFormat.R, ColorType.FLOAT32) -> "r32f"

        Pair(ColorFormat.RG, ColorType.UINT8) -> "rg8"
        Pair(ColorFormat.RG, ColorType.UINT8_INT) -> "rg8u"
        Pair(ColorFormat.RG, ColorType.SINT8_INT) -> "rg8i"
        Pair(ColorFormat.RG, ColorType.UINT16) -> "rg16"
        Pair(ColorFormat.RG, ColorType.UINT16_INT) -> "rg16u"
        Pair(ColorFormat.RG, ColorType.SINT16_INT) -> "rg16i"
        Pair(ColorFormat.RG, ColorType.FLOAT16) -> "rg16f"
        Pair(ColorFormat.RG, ColorType.FLOAT32) -> "rg32f"

        Pair(ColorFormat.RGBa, ColorType.UINT8) -> "rgba8"
        Pair(ColorFormat.RGBa, ColorType.UINT8_INT) -> "rgba8u"
        Pair(ColorFormat.RGBa, ColorType.SINT8_INT) -> "rgba8i"
        Pair(ColorFormat.RGBa, ColorType.UINT16) -> "rgba16"
        Pair(ColorFormat.RGBa, ColorType.UINT16_INT) -> "rgba16u"
        Pair(ColorFormat.RGBa, ColorType.SINT16_INT) -> "rgba16i"
        Pair(ColorFormat.RGBa, ColorType.FLOAT16) -> "rgba16f"
        Pair(ColorFormat.RGBa, ColorType.FLOAT32) -> "rgba32f"
        else -> error("unsupported layout: $format $type")
    }
