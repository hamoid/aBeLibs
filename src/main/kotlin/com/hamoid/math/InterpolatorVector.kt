package com.hamoid.com.hamoid.math

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extra.noise.random
import org.openrndr.math.*
import kotlin.math.min
    
/**
 * id: 3c7ca487-e71b-4936-95e8-f39d6d1bb937
 * description: New sketch
 * tags: #new
 */    

/**
 * Smoothly interpolate a [Vector2], [Vector3] or [Vector4]
 * towards [target]. It uses a [velocity] vector to provide
 * acceleration and deceleration.
 * The constructor accepts the initial position.
 */
class Interpolator<T>(pos: T) where T : LinearType<T>, T : EuclideanVector<T> {
    /**
     * Current value, but call [update] first on every animation frame
     */
    var current = pos
        private set

    /**
     * The [target] can be set at any time. Velocity will
     * smoothly change to reach that [target].
     */
    var target = current
        set(value) {
            if(field != value) paused = false
            field = value
        }

    var conf = InterpolatorConfig.screenMedium()

    /**
     * Optimization. Stop calculating when distance and velocity
     * are below [epsilon] = destination reached.
     */
    private var paused = true

    var velocity = current.zero
        private set

    /**
     * Update simulation and get new position
     */
    fun update() {
        if (paused) return

        var offset = target - current
        val len = offset.length
        val dist = min(conf.dampDistance, len) // 0.0 .. dampDist

        // convert dist to desired speed
        offset = offset.normalized *
                dist.map(0.0, conf.dampDistance, 0.0, conf.maxSpeed)

        var acceleration = offset - velocity
        if (acceleration.length > conf.maxAcceleration) {
            acceleration = acceleration.normalized * conf.maxAcceleration
        }

        velocity += acceleration
        if (velocity.length > conf.maxSpeed) {
            velocity = velocity.normalized * conf.maxSpeed
        }

        current += velocity

        if (offset.squaredLength < conf.epsilon && velocity.squaredLength < conf.epsilon) {
            paused = true
        }
    }

    /**
     * Teleport to [target] zeroing the current velocity
     */
    @Suppress("unused")
    fun jumpTo(target: T) {
        velocity *= 0.0
        current = target
        this.target = target
    }
}

fun main() = application {
    program {
        val i3 = Interpolator(Vector3(100.0, 100.0, 20.0))
        i3.conf.setup(20.0, 0.1, 400.0, 0.01)

        val i2 = Interpolator(Vector2(100.0, 100.0))
        i2.conf.setup(30.0, 0.15, 300.0, 0.01)

        extend {
            i3.update()
            drawer.fill = ColorRGBa.WHITE
            drawer.circle(i3.current.xy, i3.current.z)

            i2.update()
            drawer.fill = ColorRGBa.CYAN
            drawer.circle(i2.current, 5.0)
        }
        mouse.buttonDown.listen {
            i3.target = mouse.position.vector3(z = random(10.0, 100.0))
            i2.target = i3.target.xy
        }
    }
}
