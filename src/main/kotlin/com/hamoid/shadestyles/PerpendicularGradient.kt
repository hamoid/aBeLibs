package com.hamoid.shadestyles

import org.openrndr.color.ColorRGBa
import org.openrndr.draw.ColorBuffer
import org.openrndr.draw.ShadeStyle
import org.openrndr.extra.parameters.ColorParameter
import org.openrndr.extra.parameters.Description
import org.openrndr.extra.parameters.DoubleParameter
import org.openrndr.math.Vector2

@Description("Perpendicular gradient")
class PerpendicularGradient(
    color0: ColorRGBa,
    color1: ColorRGBa,
    offset: Vector2,
    rotation: Double = 0.0,
    exponent: Double = 1.0,
    noisePower: Double = 0.3
) : ShadeStyle() {

    @ColorParameter("start color", order = 0)
    var color0: ColorRGBa by Parameter()

    @ColorParameter("end color", order = 1)
    var color1: ColorRGBa by Parameter()
    var offset: Vector2 by Parameter()

    @DoubleParameter("rotation", -180.0, 180.0, order = 2)
    var rotation: Double by Parameter()

    @DoubleParameter("exponent", 0.01, 10.0, order = 3)
    var exponent: Double by Parameter()
    var texA: ColorBuffer by Parameter()

    //var texB: ColorBuffer by Parameter()
    @DoubleParameter("noise power", 0.0, 1.0, order = 4)
    var noisePower: Double by Parameter()

    init {
        this.color0 = color0
        this.color1 = color1
        this.offset = offset
        this.rotation = rotation
        this.exponent = exponent
        this.noisePower = noisePower

        fragmentPreamble = """
            //
            // Description : Array and textureless GLSL 2D simplex noise function.
            //      Author : Ian McEwan, Ashima Arts.
            //  Maintainer : ijm
            //     Lastmod : 20110822 (ijm)
            //     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
            //               Distributed under the MIT License. See LICENSE file.
            //               https://github.com/ashima/webgl-noise
            //
            
            vec3 mod289(vec3 x) {
              return x - floor(x * (1.0 / 289.0)) * 289.0;
            }
            
            vec2 mod289(vec2 x) {
              return x - floor(x * (1.0 / 289.0)) * 289.0;
            }
            
            vec3 permute(vec3 x) {
              return mod289(((x*34.0)+1.0)*x);
            }
            
            float snoise(vec2 v) {
              const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                                  0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                                 -0.577350269189626,  // -1.0 + 2.0 * C.x
                                  0.024390243902439); // 1.0 / 41.0
              // First corner
              vec2 i  = floor(v + dot(v, C.yy) );
              vec2 x0 = v -   i + dot(i, C.xx);
            
              // Other corners
              vec2 i1;
              //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
              //i1.y = 1.0 - i1.x;
              i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
              // x0 = x0 - 0.0 + 0.0 * C.xx ;
              // x1 = x0 - i1 + 1.0 * C.xx ;
              // x2 = x0 - 1.0 + 2.0 * C.xx ;
              vec4 x12 = x0.xyxy + C.xxzz;
              x12.xy -= i1;
            
              // Permutations
              i = mod289(i); // Avoid truncation effects in permutation
              vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
                + i.x + vec3(0.0, i1.x, 1.0 ));
            
              vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
              m = m*m ;
              m = m*m ;
            
              // Gradients: 41 points uniformly over a line, mapped onto a diamond.
              // The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)
            
              vec3 x = 2.0 * fract(p * C.www) - 1.0;
              vec3 h = abs(x) - 0.5;
              vec3 ox = floor(x + 0.5);
              vec3 a0 = x - ox;
            
              // Normalise gradients implicitly by scaling m
              // Approximation of: m *= inversesqrt( a0*a0 + h*h );
              m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );
            
              // Compute final noise value at P
              vec3 g;
              g.x  = a0.x  * x0.x  + h.x  * x0.y;
              g.yz = a0.yz * x12.xz + h.yz * x12.yw;
              return 130.0 * dot(m, g);
            }
            """.trimIndent()

        fragmentTransform = """
            vec2 coord = gl_FragCoord.xy - p_offset;

            float cr = cos(radians(p_rotation));
            float sr = sin(radians(p_rotation));

            float f = dot(coord, vec2(cr, sr));
            f = clamp(1.0 / pow(abs(f), p_exponent), 0.0, 1.0);
            
            vec4 color0 = p_color0;
            //color0.rgb *= color0.a;

            vec4 color1 = p_color1; 
            //color1.rgb *= color1.a;

            vec4 gradient = mix(color0, color1, f);
            
            // noise
            float scale = 0.001 + f * 0.001;
            gradient.rgb *= 1.0 + p_noisePower * snoise(coord * scale + gradient.xy); 
            
            // texture (scratches)
            float a = float(c_boundsSize.x);
            vec2 tc = mat2(cos(a), -sin(a), sin(a), cos(a)) * (c_boundsPosition.xy - 0.5) * 0.7 + 0.5;
            gradient.rgb += (texture(p_texA, tc).rgb - 0.5) * 0.1;
            
            // texture (tree shadow)
            //vec2 tc2 = gl_FragCoord.xy / u_viewDimensions;
            //gradient.rgb += texture(p_texB, tc2).rgb * 0.1;

            vec4 fn = vec4(x_fill.rgb, 1.0); // * x_fill.a;            
            
            x_fill = fn * gradient;
            
            //if (x_fill.a != 0) {
            //    x_fill.rgb /= x_fill.a;
            //}
        """
    }
}

//fun perpendicularGradient(
//    color0: ColorRGBa,
//    color1: ColorRGBa,
//    offset: Vector2 = Vector2.ZERO,
//    rotation: Double = 0.0,
//    exponent: Double = 1.0
//): ShadeStyle {
//    return PerpendicularGradient(color0, color1, offset, rotation, exponent)
//}

//fun main() = application {
//    program {
//        val fx = PerpendicularGradient(ColorRGBa.PINK.shade(0.9), ColorRGBa.BLACK, Vector2.ZERO, rotation = 90.0, exponent = 0.5)
//        fx.texA = drawImage(width, height) {
//            clear(ColorRGBa.WHITE)
//            fill = ColorRGBa.GRAY
//            bounds.scatter(60.0, 50.0, 80.0).forEach {
//                circle(it, 50.0)
//            }
//        }
//        extend {
//            drawer.shadeStyle = fx
//            drawer.bounds.grid(3, 3).flatten().forEach {
//                fx.offset = it.corner
//                drawer.rectangle(it)
//            }
//        }
//    }
//}