package com.hamoid.com.hamoid.extensions

import org.openrndr.Extension
import org.openrndr.KeyModifier
import org.openrndr.Program
import org.openrndr.color.ColorRGBa
import org.openrndr.color.rgb

/**
 * OPENRNDR extension to enable
 * CTRL+V to paste a color palette copied from any website.
 * The extension searches in the clipboard for colors
 * formatted as #123456 or 0x123456.
 */

class PastePalette : Extension {
    override var enabled: Boolean = true

    val colors = mutableListOf<ColorRGBa>()

    val size get() = colors.size

    fun isEmpty() = colors.isEmpty()
    fun isNotEmpty() = colors.isNotEmpty()

    override fun setup(program: Program) {
        println("Listening to CTRL+V")
        program.keyboard.keyDown.listen { ev ->
            if (KeyModifier.CTRL in ev.modifiers && ev.name == "v") {
                program.clipboard.contents?.let { pasted ->
                    val regex = """[#x]([0-9a-fA-F]{6})([^0-9a-fA-F]|$)""".toRegex()
                    val colors = regex.findAll(pasted).filter {
                        it.groupValues.size == 3
                    }.map {
                        it.groupValues[1]
                    }.distinct().map {
                        rgb(it)
                    }.toList()
                    this.colors.clear()
                    this.colors.addAll(colors)
                }
                ev.cancelPropagation()
            }
        }
    }
}
