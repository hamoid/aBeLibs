package com.hamoid.data

/**
 * # A minimal 3D array data structure.
 *
 * The dimensions of the array can be set in the constructor via [width], [height] and [depth].
 * A default value can be specified as [defaultVal].
 *
 * Usage:
 *
 * ```
 * val a = Array3D(2, 2, 2, 1.0)
 * a[0, 0, 0] = 2.0
 * println(a[0, 0, 0])
 * ```
 *
 */
data class Array3D<T>(
    private val width: Int,
    private val height: Int,
    private val depth: Int,
    private val defaultVal: T
) {
    private val data = MutableList(width * height * depth) { defaultVal }

    operator fun get(i: Int, j: Int, k: Int): T {
        return data[i * width * height + j * depth + k]
    }

    operator fun set(i: Int, j: Int, k: Int, v: T) {
        data[i * width * height + j * depth + k] = v
    }

    fun size() {
        data.size
    }
}
