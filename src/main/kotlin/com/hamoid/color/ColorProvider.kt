package com.hamoid.color

import org.openrndr.color.ColorRGBa

interface ColorProvider {
    var offset : Double
    fun getColor(offset: Double): ColorRGBa
    fun reset()
}
