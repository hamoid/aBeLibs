package com.hamoid.com.hamoid.anim

import com.hamoid.com.hamoid.math.Interpolator
import com.hamoid.com.hamoid.math.InterpolatorConfig
import com.hamoid.math.InterpolatorDouble
import org.openrndr.draw.Drawer
import org.openrndr.draw.isolated
import org.openrndr.math.Vector2
import org.openrndr.math.transforms.transform

/**
 * Camera interpolator. Animates camera position, rotation and scale using acceleration and deceleration.
 */
class CameraInterpolator(pos: Vector2, angle: Double = 0.0, scale: Double = 1.0) {
    /**
     * Set target position, angle and scale for the camera. Animates towards the targetAngle via the shortest
     * rotation (clockwise or CCW)
     */
    fun aim(targetPos: Vector2, targetAngle: Double, targetScale: Double) {
        pos.target = targetPos
        angle.target = targetAngle
        scale.target = targetScale
    }

    fun positionTo(targetPos: Vector2) {
        pos.target = targetPos
    }

    fun angleTo(targetAngle: Double) {
        angle.target = targetAngle
    }

    fun scaleTo(targetScale: Double) {
        scale.target = targetScale
    }

    fun positionAdd(offset: Vector2) {
        pos.target += offset
    }

    fun angleAdd(offset: Double) {
        angle.target += offset
    }

    fun scaleMultiply(factor: Double) {
        scale.target *= factor
    }

    /**
     * Call update() on every animation frame to update position, angle and scale.
     */
    fun update() {
        pos.update()
        angle.update()
        scale.update()
    }

    /**
     * Call to draw shapes using [drawer] with the camera transformations applied.
     */
    fun isolated(drawer: Drawer, draw: () -> Unit) {
        drawer.isolated {
            view *= transform {
                translate(bounds.center)
                rotate(angle.current)
                scale(scale.current)
                translate(-pos.current)
            }
            draw()
        }
    }

    private val pos = Interpolator(pos).also {
        it.conf.setup(20.0, 0.2, 350.0)
    }
    private val angle = InterpolatorDouble(angle, 360.0).also {
        it.conf.setup(5.0, 0.2, 100.0)
    }
    private val scale = InterpolatorDouble(scale).also {
        it.conf.setup(0.003, 0.001, 0.25)
    }

    /**
     * Configure the position, angle and scale interpolators
     */
    fun configure(
        posConf: InterpolatorConfig,
        angleConf: InterpolatorConfig,
        scaleConf: InterpolatorConfig
    ) {
        pos.conf.setup(posConf)
        angle.conf.setup(angleConf)
        scale.conf.setup(scaleConf)
    }
}
