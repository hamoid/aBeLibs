package com.hamoid.com.hamoid.math

data class InterpolatorConfig(
    var maxSpeed: Double,
    var maxAcceleration: Double,
    var dampDistance: Double,
    var epsilon: Double = 0.001
) {
    fun setup(
        maxSpeed: Double = 10.0,
        maxAcceleration: Double = 0.1,
        dampDistance: Double = 400.0,
        epsilon: Double = 0.001
    ) {
        this.maxSpeed = maxSpeed
        this.maxAcceleration = maxAcceleration
        this.dampDistance = dampDistance
        this.epsilon = epsilon
    }

    fun setup(config: InterpolatorConfig) =
        setup(config.maxSpeed, config.maxAcceleration, config.dampDistance, config.epsilon)

    companion object {
        fun screenSlow() = InterpolatorConfig(1.0, 0.01, 400.0)
        fun screenMedium() = InterpolatorConfig(10.0, 0.1, 400.0)
        fun screenFast() = InterpolatorConfig(50.0, 0.5, 400.0)
    }
}
