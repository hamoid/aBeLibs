package com.hamoid.geometry

import org.openrndr.math.Polar
import org.openrndr.shape.Circle
import org.openrndr.shape.ShapeContour
import org.openrndr.shape.contour

fun pillShape(c0: Circle, c1: Circle): ShapeContour {
    val t = c0.tangents(c1).flatMap { listOf(it.first, it.second) }
    return if (t.size == 4) {
        val d0 = Polar.fromVector(c0.center - c1.center).copy(radius = c0.radius)
        val d1 = Polar.fromVector(c1.center - c0.center).copy(radius = c1.radius)
        contour {
            moveTo(t[0])
            lineTo(t[1])
            circularArcTo(c1.center + d1.cartesian, t[3])
            lineTo(t[2])
            circularArcTo(c0.center + d0.cartesian, t[0])
            close()
        }

    } else ShapeContour.EMPTY
}