package com.hamoid.anim

import org.openrndr.math.map

/**
 * A real-time time tracker. Returns the current normalized [t] value
 * between [start] and [end], both in milliseconds.
 * Calling [rewind] makes [t] equal to 0.0.
 *
 */
class TimeTracker {
    private var start = 0L
    private var end = -1L
    /**
     * The current time in milliseconds
     */
    private val now
        get() = System.currentTimeMillis()

    /**
     * The normalized time value between [start] and [end]
     */
    val t
        get() = now.toDouble().map(
            start * 1.0, end * 1.0,
            0.0, 1.0, true
        )

    /**
     * Start counting down milliseconds
     */
    fun timeout(duration: Int) {
        start = now
        end = now + duration
    }

    /**
     * Reset the time tracker so [t] returns 0.0
     */
    fun rewind() {
        end = -1
    }

    fun jumpTo1() {
        start = -1
        end = 0
    }
}

//fun main() {
//    val tt = TimeTracker(100, 200)
//    println(tt.t)
//    tt.rewind()
//    println(tt.t)
//}