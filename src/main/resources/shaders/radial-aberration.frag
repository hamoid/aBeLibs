in vec2 v_texCoord0;

uniform sampler2D tex0;
uniform float strength;

out vec4 o_color;

void main() {
    vec2 uv = v_texCoord0;
    vec2 uvn = uv - 0.5;
    vec2 offset = uvn * strength;

    vec4 curr = texture(tex0, uv);

    float r = texture(tex0, uv + offset).r;
    float g = curr.g;
    float b = texture(tex0, uv - offset).b;

    o_color = vec4(r, g, b, curr.a);
}
