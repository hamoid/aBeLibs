package com.hamoid.osc

import org.openrndr.Program
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.Drawer
import org.openrndr.events.Event
import org.openrndr.extra.envelopes.ADSRTracker
import org.openrndr.extra.noise.simplex
import org.openrndr.extra.osc.OSC
import java.net.InetAddress

/**
 * OSC receiver. Audio volume tracks (access via [get], [delta], [onset]),
 * finger pressures (access via [fingers]), notes (access via the [tracker]
 * instance that was passed in the constructor).
 *
 * TODO: I would like to use ADSRTracker for /pressure, but it doesn't allow updates after
 * creation. There's only start (key down) and step (key up), but pressure currently provides
 * real time pressure. Currently I use the [fingers] map (note -> value)
 */
class AudioData(portIn: Int) {
    private val numTracks = Tr.entries.size

    private var keyboardTracker: ADSRTracker? = null
    private var drawNote: (Int, Double) -> (Double, Double, Double) -> Unit =
        { note: Int, vol: Double -> { time: Double, value: Double, pos: Double -> println("DEFAULT") } }

    /**
     * Track notes
     *
     * @param drawNote (note, vol) -> (time, value, pos) -> Unit
     */
    fun trackNotes(
        program: Program,
        attack: Double,
        decay: Double,
        sustain: Double,
        release: Double,
        drawNote: (Int, Double) -> (Double, Double, Double) -> Unit
    ) {
        keyboardTracker = ADSRTracker(program).also {
            it.attack = attack
            it.decay = decay
            it.sustain = sustain
            it.release = release
            program.extend {
                it.values().forEach { it() }
            }
        }
        this.drawNote = drawNote
    }

    // Accessed via get(), delta() and onset()
    private val bands = List(numTracks) { AudioBand() }

    // Finger pressure. Public for now
    val fingers = mutableMapOf<Int, ValueFollower>()

    // MidiFighter knobs. Public for now
    val mfKnobs = mutableMapOf<Int, Int>()

    val scene = Event<Int>()

    private val osc = OSC(InetAddress.getByName("10.42.0.1"), portIn).also {
        it.listen("/*") { address, msg ->
            when (address) {
                // Audio analysis
                "/volumes" ->
                    repeat(numTracks) { i ->
                        val vol = (msg[i] as Float).toDouble()
                        synchronized(bands) {
                            bands[i].valRecv = vol
                        }
                    }

                // Multitouch pressure tracking
                "/pressure" -> {
                    val initialNote = msg[0] as Int
                    val pressure = (msg[1] as Int) / 127.0
                    synchronized(fingers) {
                        fingers.getOrPut(initialNote) {
                            ValueFollower(pressure)
                        }.target = pressure
                    }
                }

                // Midifighter knobs
                "/cc" -> {
                    val ccNum = msg[0] as Int
                    val ccVal = msg[1] as Int
                    mfKnobs[ccNum] = ccVal
                }

                // MF playing in the keyboard. Used in 3 or 4 songs.
                "/midinote" -> keyboardTracker?.let { kt ->
                    val note = msg[0] as Int
                    val vol = (msg[1] as Int) / 127.0
                    if (vol > 0.0)
                        kt.triggerOn(note, drawNote(note, vol))
                    else
                        kt.triggerOff(note)
                }

                "/scene_launch" -> scene.trigger(0)

                // Musician
                else -> println("OSC $address ${msg.joinToString()}")
            }
        }
    }

    fun update() {
        synchronized(bands) {
            bands.forEach { it.update() }
        }
        synchronized(fingers) {
            fingers.forEach { it.value.update() }
            fingers.values.removeIf {
                it.curr < 0.01
            }
        }
    }

    // TODO: I'm missing ways to provide
    // - finger pressure
    // - midi notes

    /**
     * Returns the volume of track [tr]
     */
    operator fun get(tr: Tr) = bands[tr.ordinal].valCurr

    /**
     * Returns the volume delta of track [tr]
     */
    fun delta(tr: Tr) = bands[tr.ordinal].delta

    /**
     * Returns whether a volume onset on track [tr] above [sensitivity] was detected
     */
    fun onset(tr: Tr, sensitivity: Double) = bands[tr.ordinal].onset(sensitivity)

    /**
     * Visualize bands, fingers, tracker
     */
    fun debug(drawer: Drawer) {
        bands.forEachIndexed { i, band ->
            val v = band.valCurr
            val x = drawer.width * 0.2 + i * drawer.width * 0.05
            val y = drawer.height * 0.9 - 500.0 * v
            drawer.fill = ColorRGBa.YELLOW
            drawer.rectangle(x, y, 10.0, 6.0)
            drawer.text(Tr.entries[i].name, x, y)
        }
        fingers.forEach { (k, follower) ->
            val v = follower.curr
            val x = drawer.width * 0.2 + k * 4.0
            val y = drawer.height * 0.9 - 300.0 * v
            drawer.fill = ColorRGBa.CYAN
            drawer.rectangle(x, y, 8.0, 6.0)
        }
        keyboardTracker?.values()?.forEach { it() }
    }

    fun simulate() {
        val seconds = (System.currentTimeMillis() * 0.001) % 1000.0
        repeat(numTracks) { i ->
            val vol = simplex(i, seconds).coerceAtLeast(0.0)
            synchronized(bands) {
                bands[i].valRecv = vol
            }
        }

    }
}

//@OptIn(ExperimentalStdlibApi::class)
//fun main() = application {
//    program {
//        val ad = AudioData(57575)
//        extend {
//            ad.simulate()
//            ad.update()
//
//            Tr.entries.forEachIndexed { i, tr ->
//                drawer.rectangle(250.0 + i * 20.0, height - 50.0 - ad[tr] * 100.0, 16.0, 5.0)
//            }
//
//        }
//    }
//}
