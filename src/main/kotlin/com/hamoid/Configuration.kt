package com.hamoid

import org.openrndr.Configuration
import org.openrndr.math.IntVector2

/**
 * Used to set the window size according to a standard ISO 216
 *
 * @param n A value between -2 and 10 to set the document size. For example 4 for an A4 document.
 * @param portrait true for portrait, false for landscape
 * @param ppi pixels per inch
 */
fun Configuration.a(n: Int, portrait: Boolean = true, ppi: Double = 90.0) {
    val size = when (n) {
        -1 -> IntVector2(1682, 2378)
        -2 -> IntVector2(1189, 1682)
        0 -> IntVector2(841, 1189)
        1 -> IntVector2(594, 841)
        2 -> IntVector2(420, 594)
        3 -> IntVector2(297, 420)
        4 -> IntVector2(210, 297)
        5 -> IntVector2(148, 210)
        6 -> IntVector2(105, 148)
        7 -> IntVector2(74, 105)
        8 -> IntVector2(52, 74)
        9 -> IntVector2(37, 52)
        else -> IntVector2(26, 37)
    }
    val scaled = (size.vector2 * (ppi / 25.4)).toInt()
    if(portrait) {
        width = scaled.x
        height = scaled.y
    } else {
        width = scaled.y
        height = scaled.x
    }
}
