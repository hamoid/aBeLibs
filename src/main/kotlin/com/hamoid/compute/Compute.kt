package com.hamoid.compute

import org.openrndr.draw.*
import org.openrndr.extra.shaderphrases.preprocessShader
import org.openrndr.math.*
import java.io.File
import kotlin.reflect.KProperty0

// See computeStyle usage:
// https://github.com/openrndr/orsl/blob/master/orsl-demos/src/main/kotlin/DemoComputeStyle01.kt

// Here the declaration of ComputeStyle
// https://github.com/openrndr/openrndr/blob/d0d2eadf7c293aac927870701fe44836f6fc5b2a/openrndr-draw/src/commonMain/kotlin/org/openrndr/draw/ComputeStyle.kt#L4

/**
 * A compute-shader program which can be executed to process data in the GPU.
 */
abstract class Compute(width: Int, height: Int, val name: String) {

    @Suppress("MemberVisibilityCanBePrivate")
    val localSize = IntVector2(width, height)

    protected abstract val cs: ComputeShader

    fun fromSource(glsl: String): ComputeShader {
        val code = """
            |#version 430
            |
            |// $name - ${this::class}
            |
            |layout(local_size_x = ${localSize.x}, local_size_y = ${localSize.y}) in;
            |
            |""".trimMargin() + preprocessShader(glsl)

        File("/tmp/${this::class.simpleName}.glsl").writeText(code)

        return ComputeShader.fromCode(code, name)
    }

    abstract fun execute()
}

/**
 * Converts a `uniformList` to glsl `uniform` declarations
 */
val <E> List<E>.glsl: String
    get() = filterIsInstance<KProperty0<*>>().joinToString(
        "\n        ", "        ", postfix = "\n"
    ) {
        val type = when (it.get()) {
            is Int -> "int"
            is Double -> "float"
            else -> "unsupported"
        }
        "uniform $type ${it.name};"
    }

/**
 * Sends [uniformList] to a compute-shader
 */
fun ComputeShader.uniforms(uniformList: List<KProperty0<Any>>) {
    uniformList.forEach {
        when (val v = it.get()) {
            is Boolean -> uniform(it.name, v)
            is Int -> uniform(it.name, v)
            is Float -> uniform(it.name, v)
            is Double -> uniform(it.name, v)
            is IntVector2 -> uniform(it.name, v)
            is IntVector3 -> uniform(it.name, v)
            is IntVector4 -> uniform(it.name, v)
            is Vector2 -> uniform(it.name, v)
            is Vector3 -> uniform(it.name, v)
            is Vector4 -> uniform(it.name, v)
        }
    }
}

/**
 *  BufferPrimitiveType -> glsl type
 */
private val BufferPrimitiveType.glsl: String
    get() {
        return when (this) {
            BufferPrimitiveType.BOOLEAN -> "bool"
            BufferPrimitiveType.INT32 -> "int"
            BufferPrimitiveType.UINT32 -> "uint"
            BufferPrimitiveType.FLOAT32 -> "float"
            BufferPrimitiveType.FLOAT64 -> "double"

            BufferPrimitiveType.VECTOR2_UINT32 -> "uvec2"
            BufferPrimitiveType.VECTOR2_BOOLEAN -> "bvec2"
            BufferPrimitiveType.VECTOR2_INT32 -> "ivec2"
            BufferPrimitiveType.VECTOR2_FLOAT32 -> "vec2"
            BufferPrimitiveType.VECTOR2_FLOAT64 -> "dvec2"

            BufferPrimitiveType.VECTOR3_UINT32 -> "uvec3"
            BufferPrimitiveType.VECTOR3_BOOLEAN -> "bvec3"
            BufferPrimitiveType.VECTOR3_INT32 -> "ivec3"
            BufferPrimitiveType.VECTOR3_FLOAT32 -> "vec3"
            BufferPrimitiveType.VECTOR3_FLOAT64 -> "dvec3"

            BufferPrimitiveType.VECTOR4_UINT32 -> "uvec4"
            BufferPrimitiveType.VECTOR4_BOOLEAN -> "bvec4"
            BufferPrimitiveType.VECTOR4_INT32 -> "ivec4"
            BufferPrimitiveType.VECTOR4_FLOAT32 -> "vec4"
            BufferPrimitiveType.VECTOR4_FLOAT64 -> "dvec4"

            BufferPrimitiveType.MATRIX22_FLOAT32 -> "mat2"
            BufferPrimitiveType.MATRIX33_FLOAT32 -> "mat3"
            BufferPrimitiveType.MATRIX44_FLOAT32 -> "mat4"
        }
    }

/**
 * ShaderStorageElement -> glsl struct member variable
 *
 * Set [showArraySize] to true if you prefer `[666]` to `[]`.
 * This exists because if I specify the array size in a
 * SSBO layout block my program does not work, does not crash,
 * just stops.
 */
fun ShaderStorageElement.glsl(type: String, showArraySize: Boolean): String {
    val count = if (showArraySize) "$arraySize" else ""
    val brackets = if (arraySize > 1) "[$count]" else ""
    return "$type $name$brackets;"
}

/**
 * Convert a [ShaderStorageStruct] into a GLSL struct String
 */
fun ShaderStorageStruct.glsl(): String {
    val structMembers = elements.joinToString("\n            ") {
        when(it) {
            is ShaderStoragePrimitive -> it.glsl(it.type.glsl, true)
            is ShaderStorageStruct -> it.glsl()
            else -> throw Exception("Unknown type $it")
        }
        //it.glsl(it.type.glsl, true)
    }
    return """struct $structName {
            $structMembers
        };"""
}

/**
 * ShaderStorageFormat -> glsl layout
 */
fun ShaderStorageFormat.layout(
    bufferName: String,
    binding: Int = 0,
    showArraySize: Boolean = true
): String {
    val layoutMembers = elements.mapNotNull { ssElem ->
        when (ssElem) {
            is ShaderStoragePrimitive ->
                ssElem.glsl(ssElem.type.glsl, showArraySize)

            is ShaderStorageStruct ->
                ssElem.glsl(ssElem.structName, showArraySize)

            else -> null
        }
    }.joinToString("\n    ", "    ")
    return """
        layout(std430, binding=$binding) buffer $bufferName {
        $layoutMembers
        };"""
}

/**
 * Convert [ShaderStorageFormat] into GLSL structs String
 */
fun ShaderStorageFormat.structs(): String =
    elements.filterIsInstance<ShaderStorageStruct>()
        .joinToString("\n", postfix = "\n") { it.glsl() }

/**
 * Call `ComputeShader.buffer()` using a [NamedSSBO]
 */
fun ComputeShader.buffer(ssbo: NamedSSBO) {
    buffer(ssbo.name, ssbo.ssbo)
}

interface NamedSSBO {
    val name: String
    var ssbo: ShaderStorageBuffer
}

/**
 * Call `ComputeShader.buffer()` using a [NamedVBO]
 */
fun ComputeShader.buffer(vbo: NamedVBO) {
    buffer(vbo.name, vbo.vbo)
}

interface NamedVBO {
    val name: String
    val vbo: VertexBuffer
}
