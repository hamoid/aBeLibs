package com.hamoid.com.hamoid.svg

import com.hamoid.data.CircularArray
import com.hamoid.svg.saveToInkscapeFile
import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extra.composition.Composition
import org.openrndr.extra.composition.CompositionDrawer
import org.openrndr.extra.composition.composition
import org.openrndr.extra.composition.drawComposition
import org.openrndr.extra.noise.Random
import org.openrndr.extra.shapes.rectify.rectified
import org.openrndr.math.IntVector2
import org.openrndr.math.Vector2
import org.openrndr.math.transforms.transform
import org.openrndr.namedTimestamp
import org.openrndr.shape.Circle
import org.openrndr.shape.ShapeContour
import org.openrndr.shape.bounds
import java.io.File
import kotlin.math.PI
import kotlin.math.sin

class BWImage(val width: Int, val height: Int) {
    private val pixels = BooleanArray(width * height)

    fun clear(color: Boolean) = pixels.fill(color)

    operator fun set(x: Int, y: Int, color: Boolean) {
        if (x in 0 until width && y in 0 until height) {
            pixels[y * width + x] = color
        }
    }

    operator fun get(x: Int, y: Int) = if (x in 0 until width && y in 0 until height) {
        pixels[y * width + x]
    } else {
        false // Default to black if out of bounds
    }
}

/**
 * Remove overlaps from a Composition
 *
 * @return A new Composition where no more than one contour segment exists per pixel
 */

// Idea: Invert the order of the nested loops. Instead of contours first, then t,
// switch them around to first t, then contours, so all contours advance at once.
// But that means that each contour should have its own t and circular array.
// Create a class for that.

private class ContourDrawer(c: ShapeContour, val occupancy: BWImage, val offset: Vector2) {
    // This array is used to delay writing into buffer, avoiding colliding
    // with itself due to less-than-one-pixel movement
    val circ = CircularArray(5) { IntVector2(-5, -5) }
    val ccr = c.rectified()
    val ccl = c.length.toInt()
    var start = 0.0
    var end = 0.0
    var penDown = false
    var stepId = 0

    fun step(result: CompositionDrawer): Boolean {
        if (stepId == ccl) {
            if (penDown) {
                result.contour(ccr.sub(start, end))
                penDown = false
            }
            return true
        }

        val t = stepId / (ccl - 1.0)
        val pos = (ccr.position(t) - offset).toInt()
        val occupied = occupancy[pos.x, pos.y]

        if (occupied) {
            if (penDown) {
                result.contour(ccr.sub(start, end))
                penDown = false
            }
        } else {
            if (penDown) {
                end = t
            } else {
                start = t
                end = t
                penDown = true
            }
        }

        // store position in circular buffer
        circ.put(pos)

        // set the oldest item in array
        val oldPos = circ[circ.size - 1]
        occupancy[oldPos.x, oldPos.y] = true

        stepId++
        return false
    }
}

fun Composition.removeOverlaps(): Composition {
    // The bounds surrounding the composition with 5px margin
    val bounds = findShapes().map { it.bounds }.bounds.offsetEdges(1.0)

    // A byte buffer to store BW pixels
    val buffer = BWImage(bounds.width.toInt(), bounds.height.toInt())

    val contourDrawers = findShapes().map { it.shape.contours }.flatten().map {
        ContourDrawer(it, buffer, bounds.corner)
    }

    return drawComposition {
        var done: Boolean
        do {
            done = contourDrawers.all { it.step(this) }
        } while (!done)
    }
}

/**
 * Filter Composition by min contour length.
 */
fun Composition.filterByMinLength(len: Double) = drawComposition {
    val contours = findShapes().map { it.shape.contours }.flatten()
    val filtered = contours.filter { it.length > len }
    println("filterByMinLength: ${contours.size} -> ${filtered.size}")
    filtered.forEach { contour(it) }
}

fun Composition.filterByNoise(seed: String, scale: Vector2, threshold: Double = 0.0) = drawComposition {
    Random.seed = seed
    val contours = findShapes().map { it.shape.contours }.flatten()
    val filtered = contours.filter {
        Random.simplex(it.position(0.5) * scale) < threshold
    }
    println("filterByMinLength: ${contours.size} -> ${filtered.size}")
    filtered.forEach { contour(it) }
}

fun Composition.displaceByNoise(seed: String, scale: Vector2, threshold: Double = 0.0) = drawComposition {
    Random.seed = seed
    val contours = findShapes().map { it.shape.contours }.flatten()
    val filtered = contours.map { c ->
        val contourCenter = c.position(0.5)
        val n = Random.simplex(contourCenter * scale)
        if(n < threshold) c else {
            c.transform(transform {
                translate(contourCenter)
                rotate((n - threshold) * 360.0)
                val amt = (n - threshold) * 300.0
                translate(-contourCenter + Vector2(amt, 0.0))
            })
        }
    }
    println("filterByMinLength: ${contours.size} -> ${filtered.size}")
    filtered.forEach { contour(it) }
}
