# My OPENRNDR library

Helper classes and extensions.

There is another branch containing old Java / Processing code.

## Using this library

* Add `maven(url = "https://jitpack.io")` to the `repositories { ... }` block.
* Add `implementation("com.gitlab.hamoid:aBeLibs:v0.0.4")` to the `dependencies { ... }` block.
* Reload gradle and wait for download and index. 
* The library code should be available in your project.

