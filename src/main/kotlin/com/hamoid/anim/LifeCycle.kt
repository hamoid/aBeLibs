package com.hamoid.anim

import org.openrndr.events.Event
import kotlin.enums.EnumEntries

fun <E : Enum<E>> LifeCycle(states: EnumEntries<E>, durations: List<Int>) =
    LifeCycle(states, durations.toMutableList())

/**
 * # A sequenced, interpolated state machine.
 *
 * Events are triggered at the start and end of each transition.
 *
 * The system waits after each segment for a [wakeUp] call to continue.
 *
 * @param E The enum-entry type
 * @property states The entries in the enum
 * @property durations The transition durations for each state.
 * [durations] and [states] should have the same number of items.
 */
class LifeCycle<E : Enum<E>>(
    private val states: EnumEntries<E>,
    private val durations: MutableList<Int>
) {
    private var stateId = 0

    /**
     * The current state
     */
    val state get() = states[stateId]

    /**
     * The normalized time in the current transition.
     */
    val time get() = timeTracker.t

    /**
     * Events we can listen indicating a state change
     */
    val cycleStarted = Event<E>()
    val cycleEnded = Event<E>()

    private var timeTracker = TimeTracker()

    private enum class Status {
        // Initial state. Can't go back to this state unless restart().
        WAITING,

        // Active state. When done goes to either SLEEPING or FINISHED.
        ACTIVE,

        // Used when ACTIVE complete.
        SLEEPING,

        // Final state. Can't get out of this state unless restart().
        FINISHED
    }

    private var status = Status.WAITING

    init {
        require(states.size == durations.size) {
            "LifeCycle: states.size != durations.size"
        }
    }

    /**
     * Call this method to start the engine. Call it again
     * after receiving [cycleEnded] events to continue,
     * otherwise it waits.
     */
    fun wakeUp() {
        if (status == Status.FINISHED) return
        if (status == Status.SLEEPING || status == Status.WAITING) {
            status = Status.ACTIVE
            cycleStarted.trigger(states[stateId])
            timeTracker.timeout(durations[stateId])
        }
    }

    /**
     * Jump to the next state without completing the current one
     */
    fun skip() {
        nextCycle()
        wakeUp()
    }

    /**
     * Restart (untested). Should it trigger a finished event?
     *
     */
    fun restart() {
        stateId = 0
        status = Status.WAITING
        wakeUp()
    }

    /**
     * You can set the duration of the next segment after a .cycleEnded event.
     *
     * @param duration In milliseconds.
     */
    fun setNextDuration(duration: Int) {
        durations[stateId] = duration
    }

    /**
     * Next cycle: move to the next cycle or finish
     *
     */
    private fun nextCycle() {
        if (status == Status.FINISHED) return

        val endedState = state
        if (stateId == states.lastIndex) {
            status = Status.FINISHED
        } else {
            stateId++
            status = Status.SLEEPING
            timeTracker.rewind()
        }

        cycleEnded.trigger(endedState)
    }

    /**
     * Call once per animation frame. Detects when time reaches 1.0.
     */
    fun update() {
        if (status == Status.ACTIVE && time >= 1.0) {
            nextCycle()
        }
    }
}
