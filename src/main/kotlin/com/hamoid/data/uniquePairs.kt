package com.hamoid.data

/**
 * Returns a list of unique (non-repeated) pairs
 * of the elements in a list.
 *
 * Usage:
 *
 * ```
 * val a = listOf(2, 3, 4, 5).uniquePairs()
 * println(a)
 * // [(2, 3), (2, 4), (2, 5), (3, 4), (3, 5), (4, 5)]
 * ```
 */
fun <E> List<E>.uniquePairs(): List<Pair<E, E>> {
    val result = mutableListOf<Pair<E, E>>()

    for (i in this.indices) {
        for (j in (i + 1) until this.size) {
            result.add(Pair(this[i], this[j]))
        }
    }

    return result
}

// Previous version. Trying new approach with Pair
// because sets can't be accessed by index, making
// them inconvenient. Functionality is not exactly
// the same though: here it used the != comparator,
// the new approach does not compare, just uses
// indices. Which should be faster, but I don't know
// if in some case I might prefer comparing.
fun <E> Set<E>.uniquePairs(): Set<Set<E>> {
    val result: MutableSet<Set<E>> = LinkedHashSet()
    this.forEach { a ->
        this.forEach { b ->
            if(a != b) {
                result.add(setOf(a, b))
            }
        }
    }
    return result
}


/*
// This looks nicer but doesn't work for my purposes because
// Pair(4, 8) != Pair(8, 4)
// but
// setOf(4, 8) == setOf(8, 4)
fun <E> Collection<E>.uniquePairs(): MutableSet<Pair<E, E>> {
    val result: MutableSet<Pair<E, E>> = LinkedHashSet()
    this.forEach { a ->
        this.forEach { b ->
            if(a != b) {
                result.add(Pair(a, b))
            }
        }
    }
    return result
}
*/
