package com.hamoid.geometry

import boofcv.alg.filter.binary.BinaryImageOps
import boofcv.alg.filter.binary.ThresholdImageOps
import boofcv.struct.ConnectRule
import boofcv.struct.image.GrayS32
import boofcv.struct.image.GrayU8
import com.hamoid.com.hamoid.geometry.shortenToSimulateDepth
import com.hamoid.com.hamoid.geometry.splitAll
import com.hamoid.math.angle
import com.hamoid.math.cosEnv
import com.hamoid.math.isAngleReflex
import org.openrndr.animatable.easing.Easer
import org.openrndr.animatable.easing.Easing
import org.openrndr.application
import org.openrndr.boofcv.binding.toGrayF32
import org.openrndr.boofcv.binding.toVector2s
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.ColorBuffer
import org.openrndr.draw.VertexBuffer
import org.openrndr.draw.vertexBuffer
import org.openrndr.draw.vertexFormat
import org.openrndr.extra.noise.Random
import org.openrndr.extra.noise.uniformRing
import org.openrndr.extra.shapes.simplify.chaikinSmooth
import org.openrndr.extra.shapes.splines.CatmullRomChain2
import org.openrndr.extra.shapes.splines.toContour
import org.openrndr.math.*
import org.openrndr.shape.*
import kotlin.math.*
    
/**
 * id: afa23d31-bc70-453c-afaa-69d38f2ee9b1
 * description: New sketch
 * tags: #new
 */    

/**
 * Returns a smoothed version of a contour.
 * From openFrameworks ofPolyline.inl
 *
 * @param smoothingSize Size of the smoothing window. A value of 2 means two
 * to the left and two to the right (smoothing 5 values in total)
 * @param smoothingShape A normalized value where 0 means triangular window,
 * 1 means box window, and other values are a mix of the two.
 */
fun ShapeContour.smoothed(
    smoothingSize: Int,
    smoothingShape: Double = 0.0
): ShapeContour {
    val n = segments.size
    val sSize = clamp(smoothingSize, 0, n)
    val sShape = clamp(smoothingShape, 0.0, 1.0)

    // precompute weights and normalization
    val weights = List(sSize) {
        map(0.0, sSize * 1.0, 1.0, sShape, it * 1.0)
    }

    val result = MutableList(n) { segments[it].start }

    for (i in 0 until n) {
        var sum = 1.0 // center weight
        for (j in 1 until sSize) {
            var cur = Vector2.ZERO
            var leftPosition = i - j
            var rightPosition = i + j
            if (leftPosition < 0 && closed) {
                leftPosition += n
            }
            if (leftPosition >= 0) {
                cur += segments[leftPosition].start
                sum += weights[j]
            }
            if (rightPosition >= n && closed) {
                rightPosition -= n
            }
            if (rightPosition < n) {
                cur += segments[rightPosition].start
                sum += weights[j]
            }
            result[i] += cur * weights[j]
        }
        result[i] = result[i] / sum
    }

    return ShapeContour.fromPoints(result, closed)
}

/**
 * Adds noise to a shape contour
 *
 * @param distance How far should vertices be pushed
 * @param closed Should the returned shape be closed
 * @param zoom The scale of the simplex noise. Higher values = noisier results
 */
fun ShapeContour.noisified(
    distance: Double,
    closed: Boolean = true,
    zoom: Double = 0.002
): ShapeContour {
    return ShapeContour.fromPoints(this.segments.mapIndexed { i, it ->
        val p = it.start
        val n = it.normal(0.0)
        val noise = Random.simplex(p * zoom)
        val env = if (closed) 1.0 else cosEnv(i / (this.segments.size - 1.0))
        p + n * (3.0 * distance * noise * env)
    }, closed)
}

/**
 * Beautifies a [ShapeContour] after calling [ShapeContour.offset]
 * which may introduce artifacts and loops in certain locations
 */
fun ShapeContour.beautify(): ShapeContour {
    val equi = sampleEquidistant(segments.size / 2)
    val smooth = equi.smoothed(2)

    val equi2 = smooth.equidistantPositions(smooth.segments.size)
    val smoother = chaikinSmooth(equi2, 1, closed, 0.2)

    return ShapeContour.fromPoints(smoother, closed)
}

/**
 * Similar to [ShapeContour.offset] but with variable offset.
 *
 * [offset] is a function that returns a distance based on the normalized
 * position on the curve. The default offset does fade-in-out using cosine.
 */
fun ShapeContour.makeParallelCurve(
    offset: (Double) -> Double = { pc -> 0.5 - 0.5 * cos(pc * PI * 2) }
): ShapeContour {
    val points = mutableListOf<Vector2>()
    var prevNorm = Vector2.ZERO
    val len = segments.size.toDouble()
    segments.forEachIndexed { i, it ->
        val norm = (it.end - it.start).normalized.perpendicular()
        points.add(it.start + (norm + prevNorm).normalized * offset(i / len))
        prevNorm = norm
    }
    if (!closed) {
        points.add(segments.last().end + prevNorm * offset(1.0))
    }

    return ShapeContour.fromPoints(points, closed)
}

/**
 * Find the orientation of the longest segment of a ShapeContour
 */
@Suppress("unused")
fun ShapeContour.longestOrientation(): Double {
    val dir = longest().direction()
    return Math.toDegrees(atan2(dir.y, dir.x))
}

/**
 * Find the longest segment of a ShapeContour
 */
fun ShapeContour.longest(): Segment2D {
    return segments.maxByOrNull { it.length }!!
}

/**
 * Adds twists to a [ShapeContour] making it more wavy
 *
 * @param steps Point count for the new contour
 * @param minRadius Min radius of a `Vector2.uniformRing`
 * @param maxRadius Max radius of a `Vector2.uniformRing`
 */
fun ShapeContour.softJitter(
    steps: Int,
    minRadius: Double,
    maxRadius: Double
): ShapeContour {
    val original = this
    return contour {
        moveTo(original.segments.first().start)
        var prev =
            cursor + Vector2.uniformRing(minRadius, maxRadius) * original.length
        for (i in 1..steps) {
            val pc = i / steps.toDouble()
            val p = original.position(pc)
            val n = original.normal(pc)
            val rotation = Random.double() * maxRadius / 2.22
            val nAngle = Polar.fromVector(n).rotated(rotation - 90.0).cartesian
            val next =
                nAngle * Random.double(minRadius, maxRadius) * original.length
            curveTo(prev, p + next, p)
            prev = p - next
        }
    }
}

/**
 * Adds localized simplex-based detours to a curve.
 * [data] contains triplets with
 * - curve start percent
 * - curve end percent
 * - max offset
 * The number of entries in data control how many [ShapeContour]s are created.
 * The localized detour follows a cosine envelope (basically a fade-in-out)
 */
fun ShapeContour.localDistortion(
    data: List<Triple<Double, Double, Double>>,
    steps: Int = 100
): List<ShapeContour> {
    val parts = mutableListOf(this)
    for ((start, end, offset) in data) {
        val seg = this.sub(start, end)
        parts.add(
            ShapeContour.fromPoints(
                List(steps) {
                    val pc = it / (steps - 1.0)
                    val dry = seg.position(pc)
                    val n = Random.simplex(dry * 0.005)
                    val wet = Polar(
                        n * 360.0, // added `+ n`
                        offset * cosEnv(pc) // pc + n
                    ).cartesian
                    dry + wet
                }, false
            )
        )
    }
    return parts
}

/**
 * Used to draw a curve with dots at both ends. Use this function to
 * shorten the curve and simulate occlusion by the [a] and [b] circles.
 * Example: ( -)-----(- ) becomes (  )------(  )
 */
fun ShapeContour.eraseEndsWithCircles(a: Circle, b: Circle) =
    difference(difference(this, a.shape), b.shape).contours.first()

/**
 * Converts an open [ShapeContour] into a closed one having width.
 * The [points] list specifies the 2D locations of the points plus the
 * desired thickness for each, encoded as the .z component.
 */
fun variableWidthContour(points: List<Vector3>): ShapeContour {
    val points2d = points.map { it.xy }
    // Create a contour we can query.
    val polyline = ShapeContour.fromPoints(points2d, false)

    var normals = listOf(polyline.segments.first().normal(0.0)) +
            polyline.segments.zipWithNext { a, b ->
                (a.normal(1.0) + b.normal(0.0)).normalized
            } + polyline.segments.last().normal(1.0)

    normals = normals.mapIndexed { i, it -> it * points[i].z }

    // construct shape. First add one side, then the other in reverse.
    return ShapeContour.fromPoints(
        points2d.mapIndexed { i, p ->
            p + normals[i]
        } + points2d.reversed().mapIndexed { i, p ->
            p - normals.reversed()[i]
        }, true
    )
}

fun Segment2D.toVariableThicknessContour(
    startThickness: Double,
    endThickness: Double,
    steps: Int = 50
) = variableWidthContour(List(steps) {
    val t = it / (steps - 1.0)
    val pos = this.position(t)
    Vector3(pos.x, pos.y, t.map(0.0, 1.0, startThickness, endThickness))
})

/**
 * Convert a [ColorBuffer] to contours using boofcv. The normalized
 * [threshold] value specifies the brightness cutoff point. [internal]
 * specifies whether internal shapes should be added too.
 */
fun ColorBuffer.toContours(
    threshold: Double,
    internal: Boolean = true
): List<ShapeContour> {
    val input = this.toGrayF32()

    val binary = GrayU8(input.width, input.height)
    val label = GrayS32(input.width, input.height)
    ThresholdImageOps.threshold(input, binary, threshold.toFloat() * 255, false)
    var filtered = BinaryImageOps.erode8(binary, 1, null)
    filtered = BinaryImageOps.dilate8(filtered, 1, null)
    val contours = BinaryImageOps.contour(filtered, ConnectRule.EIGHT, label)

    val result = mutableListOf<ShapeContour>()
    contours.forEach {
        result.add(ShapeContour.fromPoints(it.external.toVector2s(), true))
        if (internal) {
            it.internal.forEach { internalContour ->
                result.add(
                    ShapeContour.fromPoints(
                        internalContour.toVector2s(),
                        true
                    )
                )
            }
        }
    }
    return result
}

/**
 * Create a [ShapeContour] spiral that starts in [p0], ends in [p1] and
 * is centered in [center]. [turns] can be positive or negative.
 */
fun spiralContour(
    p0: Vector2,
    p1: Vector2,
    center: Vector2,
    turns: Int = 1,
    easing: Easer = Easing.None.easer
): ShapeContour {
    val polars = arrayOf(
        Polar.fromVector(p0 - center),
        Polar.fromVector(p1 - center)
    )
    val actualTurns = if (turns == 0) 1 else turns
    val minIdx = if (turns < 0) 0 else 1
    val smaller = polars[minIdx]
    var greater = polars[1 - minIdx]
    val minTurn = 160 + (abs(actualTurns) - 1) * 360

    while (greater.theta - smaller.theta < minTurn) {
        greater = greater.rotated(360.0)
    }

    val steps = 100 * abs(turns)
    return ShapeContour.fromPoints(List(steps) {
        val t = it / (steps - 1.0)
        val theta = mix(smaller.theta, greater.theta, t)
        val radius = mix(smaller.radius, greater.radius, easing.ease(t, 0.0, 1.0, 1.0))
        center + Polar(theta, radius).cartesian
    }, false)
}

/**
 *
 */
fun tangentWrapConcave(a: Circle, b: Circle, radius: Double): ShapeContour {
    val points = mutableListOf<Vector2>()
    val tanCircles = a.tangentCirclesConcave(b, radius)
    tanCircles.forEach { tanCircle ->
        points.addAll(
            Segment2D(
                a.center,
                a.center - (a.center - tanCircle.center) * a.radius
            ).intersections(a.contour).map { it.position }
        )
        points.addAll(
            Segment2D(
                b.center,
                b.center - (b.center - tanCircle.center) * b.radius
            ).intersections(b.contour).map { it.position }
        )
    }
    if (points.size == 4 && (!tanCircles[0].overlap(tanCircles[1]) || b.overlap(
            a
        ))
    ) {
        return makeTangentWrapContours(
            a, b, points, radius,
            invertLarge = true,
            sweepEven = false
        )
    }
    return ShapeContour.EMPTY
}

/**
 *
 */
@Suppress("unused")
fun tangentWrapConvex(a: Circle, b: Circle, radius: Double): ShapeContour {
    val points = mutableListOf<Vector2>()
    a.tangentCirclesConvex(b, radius).forEach { circle ->
        points.addAll(
            Segment2D(
                a.center,
                a.center + (a.center - circle.center) * a.radius
            ).contour.intersections(a.contour).map { it.position }
        )
        points.addAll(
            Segment2D(
                b.center,
                b.center + (b.center - circle.center) * b.radius
            ).contour.intersections(b.contour).map { it.position }
        )
    }
    if (points.size == 4) {
        return makeTangentWrapContours(
            a, b, points, radius,
            invertLarge = false,
            sweepEven = true
        )
    }
    return ShapeContour.EMPTY
}

/**
 *
 */
private fun makeTangentWrapContours(
    a: Circle, b: Circle, points: MutableList<Vector2>, radius: Double,
    invertLarge: Boolean, sweepEven: Boolean
): ShapeContour {
    val firstLarge =
        isAngleReflex(angle(b.center, points[1], points[3])) xor invertLarge
    val secondLarge =
        !(isAngleReflex(angle(a.center, points[0], points[2])) xor invertLarge)

    return contour {
        moveTo(points[0])
        arcTo(
            radius, radius, 0.0,
            largeArcFlag = false, sweepFlag = true, end = points[1]
        )
        arcTo(
            b.radius, b.radius, 0.0,
            largeArcFlag = firstLarge, sweepFlag = sweepEven, end = points[3]
        )
        arcTo(
            radius, radius, 0.0,
            largeArcFlag = false, sweepFlag = true, end = points[2]
        )
        arcTo(
            a.radius, a.radius, 0.0,
            largeArcFlag = secondLarge, sweepFlag = sweepEven, end = points[0]
        )
        close()
    }
}

/**
 * Bends a list of [ShapeContour] with a [knife]
 */
fun List<ShapeContour>.bend(knife: ShapeContour) = this.map { contour ->
    val points = contour.equidistantPositions(contour.length.toInt())
        .toMutableList()

    if (points.isEmpty()) {
        return@map contour
    }

    // TODO: remove hardcoded 50. Should be an argument.
    // Or make it based on distance as I tried earlier.
    // So effect strength increases with distance to cut tip and
    // decreases with distance to cut-line.
    val pointCount = min(points.size, 50)
    if (knife.on(points.first(), 1.0) != null) {
        for (i in 0 until pointCount) {
            val origin = points[i].copy()
            val f = Random.simplex(points[i] * 0.03) * 200.0
//            val f = j.toDouble().map(i * 1.0, 0.0, 0.0, 1.0)
//                    .pow(2.0) * 10.0
            //val f = 1.0 / (d * 0.2)
            val nearest = knife.nearest(points[i])
            val d = nearest.position.distanceTo(points[i])
            //val n = nearest.contourT
            for (j in i downTo 0) {
                points[j] = points[j].rotate(f / (d + 1), origin)
            }
        }
    }
    if (knife.on(points.last(), 1.0) != null) {
        for (i in points.size - pointCount until points.size - 1) {
            val origin = points[i].copy()
            val f = -Random.simplex(points[i] * 0.03) * 200.0
//            val f = j.toDouble().map(i2 * 1.0, points.size - 1.0, 0.0, 1.0)
//                    .pow(2.0) * 10.0
            //val f = -1.0 / (d * 0.2)
            val nearest = knife.nearest(points[i])
            val d = nearest.position.distanceTo(points[i])
            //val n = nearest.contourT
            for (j in i until points.size) {
                points[j] = points[j].rotate(f / (d + 1), origin)
            }
        }
    }

    ShapeContour.fromPoints(points, false)
}

/**
 * Find all [ShapeContour] segment t values for [point].
 * In case of an 8 shape, it could be 4 t values.
 */
fun ShapeContour.onAll(point: Vector2, error: Double = 5.0): List<Double> {
    val result = mutableListOf<Double>()
    for (i in segments.indices) {
        val st = segments[i].on(point, error)
        if (st != null) {
            result.add((i + st) / segments.size)
        }
    }
    return result
}

// TODO: idea: what if we consider self intersections as intersection
// in which the normal differs? Also, what happens when we search for
// intersections between two identical contours?
fun ShapeContour.selfIntersections(): List<Double> {
    val result = mutableListOf<Double>()
    val intersections = mutableListOf<Vector2>()
    this.segments.forEach {
        val p = this.intersections(it)
        if (p.isNotEmpty()) {
            intersections.addAll(p.map { intersection -> intersection.position })
        }
    }
    intersections.dedupe(1.2).forEach { p ->
        // the .on() function returns only 1 point, but with self
        // intersections we may get multiple points
        val points = this.onAll(p, 1.0)
        if (points.isNotEmpty()) {
            // use random to avoid always taking the first or the second
            // used for lines that are above or below
            result.add(points.random())
        }
    }
    return result.sorted()
}

// Linear shorten. Reduces a contours length by a distance to
// the ends. The percentage differs greatly if you compare a
// straight line to a line that ends in a spiral. In the second case
// the percent location would be much higher to reach the `d` distance.
// TODO: This can now be achieved simpler by just deleting everything that
// is inside a circle of radius `d`. How to delete? ClipMode.REVERSE_DIFFERENCE?
fun ShapeContour.shorten(d: Double): ShapeContour {
    val step = 1.0 / length
    var startPc = 0.0
    var endPc = 1.0
    val start = position(startPc)
    val end = position(endPc)
    while (start.distanceTo(position(startPc)) < d && startPc < 0.4) {
        startPc += step
    }
    while (end.distanceTo(position(endPc)) < d && endPc > 0.6) {
        endPc -= step
    }
    return sub(startPc, endPc)
}


@Suppress("unused")
fun ShapeContour.removeSelfIntersections(margin: Double = 10.0):
        List<ShapeContour> {
    // find all self intersections (normalized positions in the curve)
    val intPcs = selfIntersections()

    // Show found intersections
//                intPcs.forEachIndexed { i, it ->
//                    println("$i -> $it")
//                    val p = uncut.position(it)
//                    svg.lineSegment(p - Vector2(5.0, 0.0),
//                            p + Vector2(5.0, 0.0))
//                    svg.lineSegment(p - Vector2(0.0, 5.0),
//                            p + Vector2(0.0, 5.0))
//                }

    println("Intersections: ${intPcs.size}")
    if (intPcs.isNotEmpty()) {
        val result = mutableListOf<ShapeContour>()
        for (index in intPcs.indices) {
            if (index < intPcs.size - 1) {
                result.add(
                    sub(
                        intPcs[index],
                        intPcs[index + 1]
                    ).shorten(margin)
                )
            } else {
                // Last one crosses the curve starting point
                // LAST .. END + START .. FIRST
                result.add(
                    (sub(intPcs[index], 1.0) +
                            sub(0.0, intPcs[0])).shorten(margin)
                )
            }
        }
        return result
    } else {
        return listOf(this)
    }
}

/**
 * Takes a curve and deletes the parts of it that are crossing
 * other curves. In the result it looks like one of the intersecting lines
 * is above and the other below.
 * Not perfect: struggles with self intersections.
 */
fun MutableList<ShapeContour>.removeIntersections(margin: Double) {
    splitAll()
    shortenToSimulateDepth(margin)
}

/**
 * Smooths out a [ShapeContour] by making sure all curve control points
 * are symmetric (so cp0-point-cp1 form a straight line).
 * [roundness] is a function that takes an index and returns a pair of
 * [Double] specifying how far away the previous and next control points are.
 * [symmetrize] returns a pair containing the new [ShapeContour] and also
 * a list of [LineSegment] in case one wants to draw the tangent lines.
 */
fun ShapeContour.symmetrize(
    roundness: (Int) -> Pair<Double, Double> = { Pair(0.333, 0.333) }
):
        Pair<ShapeContour, List<LineSegment>> {
    val visibleTangents = mutableListOf<LineSegment>()
    val tangents = segments.mapIndexed { i, curr ->
        val next = segments[(i - 1 + segments.size) % segments.size]
        (curr.direction()).mix(next.direction(), 0.5).normalized
    }
    val newSegments = segments.mapIndexed { i, currSegment ->
        val sz = segments.size
        val len = currSegment.length
        val iNext = (i + 1 + sz) % sz
        val c0 = currSegment.start +
                tangents[i] * len * roundness(i).first
        val c1 = currSegment.end -
                tangents[iNext] * len * roundness((i + 1) % sz).second
        visibleTangents.add(LineSegment(currSegment.start, c0))
        visibleTangents.add(LineSegment(c1, currSegment.end))
        visibleTangents.add(
            LineSegment(
                currSegment.start,
                currSegment.end
            )
        )
        Segment2D(currSegment.start, c0, c1, currSegment.end)
    }
    return Pair(ShapeContour(newSegments, closed), visibleTangents)
}

/**
 * Similar to [ShapeContour.symmetrize] but without returning tangent lines.
 */
fun ShapeContour.symmetrizeSimple(
    roundness: (Int) -> Pair<Double, Double> = { Pair(0.333, 0.333) }
):
        ShapeContour {
    val tangents = segments.mapIndexed { i, curr ->
        val next = segments[(i - 1 + segments.size) % segments.size]
        (curr.direction()).mix(next.direction(), 0.5).normalized
    }
    val newSegments = segments.mapIndexed { i, currSegment ->
        val sz = segments.size
        val len = currSegment.length
        val iNext = (i + 1 + sz) % sz
        val c0 = currSegment.start +
                tangents[i] * len * roundness(i).first
        val c1 = currSegment.end -
                tangents[iNext] * len * roundness((i + 1) % sz).second
        Segment2D(currSegment.start, c0, c1, currSegment.end)
    }
    return ShapeContour(newSegments, closed)
}


/**
 * Creates a deformed circle centered at [pos] and with radius [radius]
 */
fun circleish(pos: Vector2, radius: Double, angularOffset: Double = 0.0) =
    CatmullRomChain2(List(5) {
        it * 72 + angularOffset + Random.double0(50.0)
    }.map {
        Polar(it, radius).cartesian + pos
    }, 0.5, true).toContour()

/**
 * Creates a deformed circle centered at [center] and with radius [radius]
 */
fun circleish2(
    center: Vector2,
    radius: Double,
    pointCount: Int = 100,
    orientation: Double = 0.0,
    noiseScale: Double = 0.1,
    noiseFreq: Double = 1.0
): ShapeContour {
    return ShapeContour.fromPoints(
        List(pointCount) { i ->
            val angle = i / pointCount.toDouble()
            val cycle = sin(angle * PI * 2 + orientation) * noiseFreq
            val maxOffset = radius * noiseScale
            val offset =
                Random.simplex(cycle.pow(5.0), center.x, center.y) * maxOffset
            center + Polar(angle * 360, radius + offset).cartesian
        }, true
    )
}

/**
 * Creates a [ShapeContour] between two points with some randomness
 */
fun ShapeContour.Companion.bentFromPoints(a: Vector2, b: Vector2, rnd: Double):
        Segment2D {
    val d = a.distanceTo(b) * rnd
    return Segment2D(
        a,
        a.mix(b, 0.333) + Polar(
            500 * Random.simplex(a * 0.01),
            d * Random.simplex(a.yx * 0.01).absoluteValue
        ).cartesian,
        a.mix(b, 0.666) + Polar(
            500 * Random.simplex(b * 0.01),
            d * Random.simplex(b.yx * 0.01).absoluteValue
        ).cartesian,
        b
    )
}


fun Segment2D.mix(other: Segment2D, mix: Double) =
    other * mix + this * mix

/**
 * Remove contour points if they do not contribute
 * enough to direction change.
 */
fun ShapeContour.simplified(threshold: Double = 0.5): ShapeContour {
    val points = mutableListOf<Vector2>()

    var anchor = Vector2.INFINITY
    var dirLast =Vector2.INFINITY
    segments.forEach {
        if (points.isEmpty()) {
            anchor = it.start
            dirLast = it.direction()
            points.add(anchor)
        } else {
            val dirCurr = (it.end - anchor).normalized
            // NOTE: at the end I added this constant to solve an issue
            // with very long series of points going in the same direction
            // and then turning. The angle from anchor to that last point
            // wouldn't change much because of the distance.
            // Maybe what we want instead is a distance to the original curve?
            // So we go straight until the deviation is too large
            // FIXME
            val k = 1.0 / (1.0 + it.end.distanceTo(anchor) * 0.1)
            val dirDiff = acos(dirCurr.dot(dirLast))
            if (dirDiff > threshold * k) {
                anchor = it.start
                dirLast = dirCurr
                points.add(anchor)
            }
        }
    }
    if(!closed) {
        points.add(segments.last().end)
    }
    //println("before ${segments.size} after ${simpler.segments.size}")
    return ShapeContour.fromPoints(points, true)
}

/**
 * # Antijaggy
 *
 * Returns simplified [ShapeContour] in which consecutive segments
 * with equal direction are removed. To be applied on contours
 * acquired from boofcv, which are typically pixel-bound
 * (all steps are either horizontal or vertical)
 */
fun ShapeContour.antijaggy(): ShapeContour {
    val extended = mutableListOf<Segment2D>()

    segments.forEach { curr ->
        if (extended.isEmpty() || (curr.direction() - extended.last().direction()).squaredLength > 0.001)
            extended.add(curr)
        else {
            extended[extended.size - 1] = extended.last().copy(end = curr.end)
        }
    }
    val bridged = extended.mapIndexed { i, segment ->
        if (i == extended.size - 1 || segment.end == extended[i + 1].start) segment else
            segment.copy(end = extended[i + 1].start)
    }
    return ShapeContour(bridged, closed)
}

/**
 * Reduces the number of ShapeContour items on a list by
 * merging them into longer ShapeContours. The [minDist] value specifies
 * the distance threshold between contour end-points under which they are
 * merged into one.
 */
fun List<ShapeContour>.concatenate(minDist: Double = 0.1): List<ShapeContour> {
    if (this.isEmpty()) return listOf()

    val source = this.toMutableList()
    val result = mutableListOf<ShapeContour>()
    var found: ShapeContour? = null
    var builder = source.removeLast()

    while (source.isNotEmpty()) {
        val start = builder.position(0.0)
        val end = builder.position(1.0)

        for (other in source) {
            val otherStart = other.position(0.0)
            val otherEnd = other.position(1.0)
            when {
                otherStart.distanceTo(end) < minDist -> {
                    found = other
                    builder += found
                    break
                }

                otherEnd.distanceTo(end) < minDist -> {
                    found = other
                    builder += found.reversed
                    break
                }

                otherStart.distanceTo(start) < minDist -> {
                    found = other
                    builder = builder.reversed + found
                    break
                }

                otherEnd.distanceTo(start) < minDist -> {
                    found = other
                    builder = found + builder
                    break
                }
            }
        }
        if(found != null) {
            source.remove(found)
            found = null
        } else {
            result.add(builder.copy())
            builder = source.removeLast()
        }
    }
    result.add(builder)

    return result
}

/**
 * convert a `List<Segment>` into a `List<ShapeContour>`
 * by grouping contiguous segments that start where previous ones end.
 * This reduces pen up/down movements when plotting with the Axidraw.
 */
fun List<Segment2D>.fuse(): List<ShapeContour> {
    val bucket = mutableListOf<Segment2D>()
    val result = mutableListOf<ShapeContour>()

    this.forEach {
        if (bucket.isEmpty() || bucket.last().end.squaredDistanceTo(it.start) < 10E-6) {
            bucket.add(it)
        } else {
            result.add(ShapeContour(bucket.toMutableList(), false))
            bucket.clear()
            bucket.add(it)
        }
    }
    result.add(ShapeContour(bucket, false))

    return result
}

/**
 * Create a [ShapeContour] arc specifying the [center],
 * [start] point and [end] point.
 *
 * The distance from [center] to both [start] and [end] must be equal,
 * otherwise an empty contour is returned.
 */
fun arcFromCenter(center: Vector2, start: Vector2, end: Vector2) =
//    if (center.squaredDistanceTo(start) != center.squaredDistanceTo(end))
//        ShapeContour.EMPTY
//    else
        contour {
            val d = center.distanceTo(start)
            val side = ((start.x - center.x) * (end.y - center.y) -
                    (start.y - center.y) * (end.x - center.x)) > 0

            moveTo(start)
            arcTo(d, d, 0.0, !side, true, end)
        }



fun main() = application {
    program {
        val p = Rectangle.fromCenter(
            drawer.bounds.center, 250.0
        ).contour.equidistantPositions(40).dropLast(1)
        val c = ShapeContour.fromPoints(p, true).antijaggy()
        extend {
            drawer.clear(ColorRGBa.WHITE)
            drawer.fill = null
            drawer.contour(c)
            drawer.circles(c.segments.map { it.start }, 5.0)
        }
    }
}



/**
 * Converts a [ShapeContour] into a [VertexBuffer]. It takes
 * [samples] samples of the Segment. The thickness can
 * be constant: `contour.toMesh(50) { 10.0 }`
 * or variable: `contour.toMesh(50) { t -> 10.0 * t }`
 * or even:     `contour.toMesh(30) { t -> cos(t * 3.14159) * 10 + 5`
 */
fun ShapeContour.toMesh(
    samples: Int,
    thickness: (Double) -> Double = { 5.0 }
): VertexBuffer {
    val actualSamples = samples + if (closed) 1 else 0
    val vb = vertexBuffer(vertexFormat {
        position(3)
        textureCoordinate(2)
    }, actualSamples * 2)

    vb.put {
        repeat(actualSamples) {
            val t = it / samples.toDouble()
            val t1 = t % 1.0
            val pos = position(t1)
            val n = normal(t1) * 0.5
            write((pos + n * thickness(t1)).xy0)
            write(Vector2(t, 0.0))
            write((pos - n * thickness(t1)).xy0)
            write(Vector2(t, 1.0))
        }
    }
    return vb
}



/**
 * Converts a [Segment2D] into a [VertexBuffer]. It takes
 * [samples] samples of the Segment. The thickness can
 * be constant: `segment.toMesh(50) { 10.0 }`
 * or variable: `segment.toMesh(50) { t -> 10.0 * t }`
 * or even:     `segment.toMesh(30) { t -> cos(t * 3.14159) * 10 + 5 }`
 */
fun Segment2D.toMesh(
    samples: Int,
    thickness: (Double) -> Double = { 5.0 }
): VertexBuffer {
    val vb = vertexBuffer(vertexFormat {
        position(3)
        textureCoordinate(2)
    }, samples * 2)

    vb.put {
        repeat(samples) {
            val t = it / (samples - 1.0)
            val pos = position(t)
            val n = normal(t) * 0.5
            write((pos + n * thickness(t)).xy0)
            write(Vector2(t, 0.0))
            write((pos - n * thickness(t)).xy0)
            write(Vector2(t, 1.0))
        }
    }
    return vb
}
