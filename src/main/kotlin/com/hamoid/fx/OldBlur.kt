package com.hamoid.com.hamoid.fx

import org.openrndr.application
import org.openrndr.color.rgb
import org.openrndr.draw.*
import org.openrndr.extra.fx.blur.DirectionalBlur
import org.openrndr.shape.Rectangle
    
/**
 * id: a9c40a26-e058-449e-bd55-454021cb6a12
 * description: New sketch
 * tags: #new
 */    

/**
 * Old blur
 *
 * Filter that applies a radial DirectionalBlur and a radial
 * chromatic aberration to the input ColorBuffer.
 */
class OldBlur : Filter() {
    private val fBlur = DirectionalBlur().also {
        it.window = 10
        it.spread = 2.0
    }
    val radialAberration = RadialAberration()
    val radialJitter = RadialJitter().also {
        it.minDist = 0.3
        it.maxDist = 0.7
        it.minBlur = 0.1
        it.maxBlur = 0.5
    }
    private var blurMap: ColorBuffer? = null
    private var blurred: ColorBuffer? = null
    private var seed = 0.0

    private fun initBuffers(src: ColorBuffer) {
        blurMap?.let {
            if (!it.isEquivalentTo(src, ignoreType = true)) {
                it.destroy()
            }
        }
        if (blurMap == null) {
            blurMap = src.createEquivalent(type = ColorType.FLOAT32)
        }

        blurred?.let {
            if (!it.isEquivalentTo(src)) {
                it.destroy()
            }
        }
        if (blurred == null) {
            blurred = src.createEquivalent()
        }
    }

    override fun apply(source: Array<ColorBuffer>, target: Array<ColorBuffer>, clip: Rectangle?) {
        initBuffers(target[0])

        seed = (seed + 0.001) % 1.0
        radialJitter.apply(emptyArray(), blurMap!!)
        fBlur.apply(arrayOf(source[0], blurMap!!), blurred!!)
        radialAberration.apply(blurred!!, target)
    }
}

/** Test OldBlur */
fun main() = application {
    program {
        val w = width + 20
        val h = height + 20
        val oldBlur = OldBlur()
        val rt = renderTarget(w, h) {
            colorBuffer()
        }
        val result = colorBuffer(w, h)
        drawer.isolatedWithTarget(rt) {
            for (r in 400 downTo 10 step 40) {
                fill = rgb((r * 1.3579) % 1.0)
                circle(bounds.center, r * 1.0)
            }
        }
        extend {
            oldBlur.apply(rt.colorBuffer(0), result)
            drawer.image(result, -10.0, -10.0)
        }
    }
}
